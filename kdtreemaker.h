#ifndef KDTREEMAKER_H
#define KDTREEMAKER_H

#include "pipeline.h"
#include "raymarch/KdTree2D.h"
#include <fstream>
#include <sstream>
#include <vector>
#include <list>
#include <iostream>
#include "raymarch/Device.h"

std::vector<Qin::LineSegment> read_prims(const char* fileName)
{
    std::vector<Qin::LineSegment> primSoup;
    std::ifstream ifs(fileName);
    while( ifs.good() ){
        Qin::Vec2 v1, v2;
        ifs >> v1.x >> v1.y >> v2.x >> v2.y;
        primSoup.push_back( Qin::LineSegment(v1, v2) );
        //std::cout << primSoup.back().start() << "-->"<< primSoup.back().end() << std::endl;
    }
    return primSoup;
}

std::string draw_tree_node( const Qin::KdTree2DNode& node, const Qin::KdTree2DNode* parent, bool style, bool drawprim = true)
{
    std::stringstream ss;
    float r, b, g = 0.0f;
    if( style){
        r = 1.0;
        b = 0.0;
    }
    else{
        r = 0.0;
        b = 1.0;
    }
    ss << set_pen(r, g, b, 1, DOT);
    if( !node.leaf()){
        Qin::Split s = node.split();
        Qin::Vec2 v1 = node.aabb().lowerbound();
        Qin::Vec2 v2 = node.aabb().upperbound();
        v1[s.axis] = s.pos;
        v2[s.axis] = s.pos;
        ss << draw_line(v1.x, v1.y, v2.x, v2.y);
    }
    else if( drawprim){
        int pen = SOLID;
        if( parent->left_child() == &node)
            pen = DASH;
        ss << set_pen(0.0, 0.0, 0.0, 1, pen);
        for( Qin::PrimitivePtrList::const_iterator it = node.prim_list().begin();
             it != node.prim_list().end(); ++it)
        {
            Qin::LineSegment seg = (*it)->line_seg();
            ss << draw_line(seg.start().x, seg.start().y, seg.end().x, seg.end().y);
        }
    }
    //std::cout << ss.str() << std::endl;
    return ss.str();
}

void draw_tree(std::ofstream& ofs, const Qin::KdTree2DNode* proot, const Qin::KdTree2DNode* parent,  int style)
{
    if(proot){
        ofs << draw_tree_node(*proot, parent, style);
        draw_tree(ofs, proot->left_child(), proot, 1 - style);
        draw_tree(ofs, proot->right_child(), proot, 1 - style);
    }
}

void create_drawing_pipeline_file(const char *fileName,
                                  const std::list<Qin::Device*>& devList,
                                  const Qin::KdTree2D &kdTree)
{
    std::ofstream ofs(fileName);
    ofs << enable_antialiase(true);
    ofs << set_pen(0, 0, 0, 3, SOLID);
    Qin::Vec2 lb = kdTree.root()->aabb().lowerbound();
    Qin::Vec2 ub = kdTree.root()->aabb().upperbound();
    ofs << draw_rect(lb.x, lb.y, ub.x, ub.y);
    draw_tree(ofs, kdTree.root(), NULL, 1);
    ofs.close();
}
/*
void create_drawing_pipeline_file(const char* fileName,
                                  const Qin::Primitive* beg,
                                  const Qin::Primitive* end,
                                  const Qin::KdTree2D& kdTree)
{
    std::ofstream ofs(fileName);
    ofs << enable_antialiase(true);
    ofs << set_pen(0, 0, 1.0, 1.0, SOLID);
    for( const Qin::Primitive* it = beg; it != end; ++it){
        const Qin::LineSegment seg = it->line_seg();
        Qin::Vec2 n1 = it->vertex() + it->normal()*20;
        Qin::Vec2 center_point = 0.5*(seg.start()+seg.end());
        Qin::Vec2 n2 = center_point + it->normal_at(0.5)*10;
        ofs << draw_line( seg.start().x, seg.start().y, seg.end().x, seg.end().y);
        ofs << draw_line( seg.start().x, seg.start().y, n1.x, n1.y);
        ofs << draw_line( center_point.x, center_point.y, n2.x, n2.y);
    }
    ofs << set_pen(0, 0, 0, 2, SOLID);
    Qin::Vec2 lb = kdTree.root()->aabb().lowerbound();
    Qin::Vec2 ub = kdTree.root()->aabb().upperbound();
    ofs << draw_rect(lb.x, lb.y, ub.x, ub.y);
    draw_tree(ofs, kdTree.root(), 1);
    ofs.close();
}

void create_drawing_pipeline_file(const char* fileName,
                                  const std::vector<Qin::LineSegment>& primSoup,
                                  const Qin::KdTree2D& kdTree)
{
    std::ofstream ofs(fileName);
    ofs << enable_antialiase(true);
    ofs << set_pen(0, 0, 1.0, 1.0, SOLID);
    for( std::vector<Qin::LineSegment>::const_iterator it = primSoup.begin();
         it != primSoup.end(); ++it){
        const Qin::LineSegment& seg = *it;
        ofs << draw_line( seg.start().x, seg.start().y, seg.end().x, seg.end().y);
    }
    ofs << set_pen(0, 0, 0, 2, SOLID);
    Qin::Vec2 lb = kdTree.root()->aabb().lowerbound();
    Qin::Vec2 ub = kdTree.root()->aabb().upperbound();
    ofs << draw_rect(lb.x, lb.y, ub.x, ub.y);
    draw_tree(ofs, kdTree.root(), 1);
    ofs.close();
}


void create_drawing_pipeline_file(const char* fileName,
                                  const std::vector<Qin::LineSegment>& primSoup)
{
    std::ofstream ofs(fileName);
    ofs << enable_antialiase(true);
    ofs << set_pen(0, 0, 1.0, 1.0, SOLID);
    for( std::vector<Qin::LineSegment>::const_iterator it = primSoup.begin();
         it != primSoup.end(); ++it){
        const Qin::LineSegment& seg = *it;
        ofs << draw_line( seg.start().x, seg.start().y, seg.end().x, seg.end().y);
    }
    ofs.close();
}
*/
#endif // KDTREEMAKER_H
