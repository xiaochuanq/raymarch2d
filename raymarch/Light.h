#ifndef LIGHT_H
#define LIGHT_H

#include <vector>
#include "qmath/QMath.h"

namespace Qin{

struct Photon
{
    Photon(int wl, real i) : waveLength(wl), intensity(i) { }
    int waveLength;
    real intensity;
};

struct LightRay
{
    LightRay(size_t waveLength, real intensity, const Vec2& origin, const Vec2& dir)
        : photon(waveLength, intensity), ray(origin, dir) {}
    inline size_t wavelength() const { return photon.waveLength; }
    inline real intensity() const { return photon.intensity; }
    Photon photon;
    Ray ray;
};

struct DrawableSegment
{
    DrawableSegment(real width, const Color3& color0, const Color3& color1,
                    const Vec2& pos2d0, const Vec2& pos2d1, real depth) :
        lineWidth(width), clr0(color0), clr1(color1),
        pos0(pos2d0.x, pos2d0.y, depth), pos1(pos2d1.x, pos2d1.y, depth){}

    real lineWidth;
    Color3 clr0;
    Color3 clr1;
    Vec3 pos0;
    Vec3 pos1;
};

inline DrawableSegment light_ray_to_drawable_segment( real thickNess, const LightRay& lightRay, real length)
{
    Color3 color0 = wavelength2rgb_real(lightRay.wavelength()) * lightRay.intensity();
    // TODO: use attenuate here to set color1
    LineSegment seg = lightRay.ray.segment(0, length);
    return DrawableSegment(thickNess, color0, color0, seg.start(), seg.end(), (real)lightRay.wavelength() );
}

} // end of namespace Qin
#endif // LIGHT_H
