#ifndef PRIMITIVE_H
#define PRIMITIVE_H
#include "qmath/QMath.h"

namespace Qin
{
class Device;

class Primitive
{
    friend class Device;
public:
    inline const Vec2& vertex() const { return point_; }

    inline const Vec2& normal() const { return normal_; }

    inline Vec2 normal_at(real t) const
    {
        assert( in_range(t, real(0.0), real(1.0) ) );
        Vec2 normal = t*(this+1)->normal() + (1-t)*this->normal();
        return normal.normalized_copy();
    }

    inline Vec2 position_at(real t) const
    {
        assert( in_range(t, real(0.0), real(1.0) ) );
        return t*(this+1)->vertex() + (1-t)*this->vertex();
    }

    inline LineSegment line_seg() const
    {
        return LineSegment( this->vertex(), (this+1)->vertex() );
    }

    inline void end_points(Vec2& v1, Vec2& v2) const
    {
        v1 = this->vertex();
        v2 = (this+1)->vertex();
    }

    inline Vec2 end_point(size_t i) const
    {
        return  (this+i)->vertex();
    }

    inline const AABB& aabb() const { return aabb_; }

    inline const Device& device() const { return *devicePtr_; }
private:
    Vec2        point_;
    Vec2        normal_;
    AABB        aabb_;
    Device*     devicePtr_;
};

}
#endif // PRIMITIVE_H
