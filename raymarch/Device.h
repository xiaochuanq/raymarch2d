#ifndef DEVICE_H
#define DEVICE_H
#include <cassert>
#include <vector>
#include <list>
#include "qmath/QMath.h"
#include "Primitive.h"
#include "qmath/Discretize.h"

namespace Qin
{

typedef void Material; // pending implementation

class Device{
    friend class DeviceFactory;
public:
    inline bool active() const { return active_; }
    inline void activate(bool a=true) { active_ = a; }

    inline size_t size() const { return size_; }
    inline const Vec2* points() const { return points_; }
    inline Vec2* points() { return points_; }
    inline const Vec2& point(size_t idx) const { return points_[idx]; }
    inline Vec2& point(size_t idx) { return points_[idx]; }

    inline const Vec2* normals() const { return normals_; }
    inline Vec2* normals() { return normals_; }
    inline const Vec2& normal(size_t idx) const { return normals_[idx]; }
    inline Vec2& normal(size_t idx) { return normals_[idx]; }

    //inline const Material* material(){ assert(material_); return material_; }

    void transform_primitive(const Vec2& t, real a) {
        real s = std::sin(a);
        real c = std::cos(a);
        for( size_t i = 0; i < size_; ++i){
            primitives_[i].point_ = transform(points_[i], t, s, c);
            primitives_[i].normal_ = rotate(normals_[i], s, c);
            primitives_[i].devicePtr_ = this;
        }
        for( size_t i = 0; i < size_; ++i){
          primitives_[i].aabb_ = aabb(primitives_[i].vertex(), primitives_[i+1].vertex());
        }
    }

    inline const Primitive* primitive_beg() const // The interface for kdTree building
    {
        return primitives_;
    }

    inline const Primitive* primitive_end() const // The interface for kdTree building
    {
        return primitives_ + size_ - 1; // the last one is a dummy primitive
    }

protected:
    Device(size_t numSeg, Vec2* vertex_buffer, Vec2* normal_buffer, Primitive* prim_buffer, bool active = false ) :
        size_(numSeg),
        points_(vertex_buffer),
        normals_(normal_buffer),
        primitives_(prim_buffer),
        material_(0),
        active_(active)
    {}

private:
    size_t size_;
    Vec2* points_;  // the polyline or polygon
    Vec2* normals_; // the normals at each ployline point
    Primitive* primitives_;
    Material* material_;
    bool active_;
};

class DeviceFactory
{
public:
    enum{
        CIRCLE_CLIP,
        POLYBOLA_CLIP,
        POLYGON,
        LINE_SEGMENT
    };
public:
    DeviceFactory(size_t bufferSize = 0):
        capacity_(0),
        head_idx_(0),
        vertex_pool_(0),
        normal_pool_(0),
        primitive_pool_(0)
    {
        if(bufferSize > 0)
            allocate_buffer(bufferSize);
    }

    ~DeviceFactory()
    {
        release_buffer();
    }

    inline const Primitive* primitive_pool_beg() const // The interface for kdTree building
    {
        return primitive_pool_;
    }

    inline const Primitive* primitive_pool_end() const // The interface for kdTree building
    {
        return primitive_pool_ + head_idx_ - 1; // the last one is a dummy primitive
    }

    void reset_buffer(){
        head_idx_ = 0;
    }

    void release_buffer(){
        capacity_ = 0;
        head_idx_ = 0;
        delete vertex_pool_;
        delete normal_pool_;
        delete primitive_pool_;
        vertex_pool_ = NULL;
        normal_pool_ = NULL;
        primitive_pool_ = NULL;
    }

    void allocate_buffer(size_t n)
    {
        assert(vertex_pool_ == 0 && normal_pool_ == 0 && primitive_pool_ == 0);
        capacity_ = n;
        vertex_pool_ = new Vec2[n];
        normal_pool_ = new Vec2[n];
        primitive_pool_ = new Primitive[n];
    }

    std::list<Device*>* create_devices()
    {

        std::list<Device*>* devList = new std::list<Device*>();
        Circle c(200);

        size_t old_head_idx = head_idx_;
        size_t elemNum1 = discretize( c, vertex_pool_ + head_idx_, normal_pool_ + head_idx_, Vec2(1, -1), 33);
        head_idx_ += elemNum1;
        std::vector<Vec2> polyline;
        polyline.push_back(Vec2(200*cos(-1), 200*sin(-1)));
        polyline.push_back(Vec2(210, 200*sin(-1)));
        polyline.push_back(Vec2(210, 200*sin(1)));
        polyline.push_back(Vec2(200*cos(1), 200*sin(1)));
        size_t elemNum2 =  discretize(polyline, vertex_pool_ + head_idx_, normal_pool_ + head_idx_);
        head_idx_ += elemNum2;
        transform_buffer(old_head_idx, elemNum1 + elemNum2, Vec2( -(200*cos(1)+210)/2, 0), 0 );
        Device* pd = new Device(elemNum1 + elemNum2, vertex_pool_ + old_head_idx, normal_pool_ + old_head_idx, primitive_pool_ + old_head_idx, true);
        devList->push_back(pd);

        old_head_idx = head_idx_;
        elemNum1 = discretize( c, vertex_pool_ + head_idx_, normal_pool_ + head_idx_, Vec2(-1, 1), 33);
        head_idx_ += elemNum1;
        polyline.clear();
        polyline.push_back(Vec2(200*cos(1), 200*sin(1)));
        polyline.push_back(Vec2(200*cos(-1), 200*sin(-1)));
        elemNum2 = discretize(polyline, vertex_pool_ + head_idx_, normal_pool_ + head_idx_);
        head_idx_ += elemNum2;
        transform_buffer(old_head_idx, elemNum1 + elemNum2, Vec2( -(200*cos(1)+200)/2, 200), 0);
        pd = new Device(elemNum1 + elemNum2, vertex_pool_ + old_head_idx, normal_pool_ + old_head_idx, primitive_pool_ + old_head_idx, true);
        devList->push_back(pd);

        return devList;
    }

    size_t size(){
        return head_idx_;
    }

private:
    void transform_buffer(size_t beg, size_t n, const Vec2 t, real a)
    {
        real s = std::sin(a);
        real c = std::cos(a);
        transform(vertex_pool_+beg, n, t, s, c);
        rotate(normal_pool_+beg, n, s, c);
    }

private:
    size_t capacity_;
    size_t head_idx_;
    Vec2* vertex_pool_;
    Vec2* normal_pool_;
    Primitive* primitive_pool_;
};

} // end of name space
#endif // DEVICE_H
