/*
 *
 * The goal of my project is to implemented a Geometrical Optics 2D game engine on mobile devices, where memory and performance
 * are the major concer.
 *
 * Fast ray and line segment intersection computation with a controllable footprint is the key of the project. This file implementes
 * a kd-tree and the ray intersection algorithms. It servers the "hub" of all other algorithms - intersection, geometric optics,
 * wavelenght to color arithmetics, OpenGL shader rendering, optical device modeling and small object allocation etc. are all organzied
 * around this component.
 *
 * The algorithm is inspired by Wald, I.'s "On fast Construction of SAH-based Bounding Volume Hierarchies".
 * Besides the ideas of binning, generalization is made as
 *   3D Bounding Volume => 2D Axis Aligned Bounding Box
 *   3D Surface => 2D Line Segment
 *   3D Surface Area Heuristics => 2D Perimeter Length Heuristics
 *
 * To improve performance and reduce memory usage,
 * 1) Lazy build. The kd-tree nodes are build only when the corresponding areas are hit.
 * 2) Careful selection of data structure. forward_list, unique_ptr, and only necessary flyweight etc.
 * 3) Tunable granularity. users can specify the stop criterion of splitting nodes.
 *
 * This demo file is a combination of several sources in original project.
 */

#ifndef KDTREE2D_HPP
#define KDTREE2D_HPP

#include <cstring>
#include <forward_list>
#include <memory>
#include "Primitive.h"
#include "qmath/QMath.h"
#include "qmath/Intersection.h"

namespace Qin{

// Basically, a Primitive is defined as a line segment with other information such as its normal vector and bounding boxes.
typedef std::forward_list<const Primitive*> PrimitivePtrList;

//////////////////////// KdTree2DNode Split Finder ///////////////////////////////////////////////////////////////////////////////

// The interface of the kd-tree node split finder
template<typename InputIterator>
class SplitEvaluator
{
public:
    virtual Split find_split(InputIterator first, InputIterator last, const AABB& box) = 0;
};

// The implementation of Wald's Binning algorithm
class BinCountingSplitEvaluator : public SplitEvaluator<PrimitivePtrList::const_iterator>
{
public:
    BinCountingSplitEvaluator()
    {
    }

    Split find_split(PrimitivePtrList::const_iterator first, PrimitivePtrList::const_iterator last, const AABB& box)
    {
        assert(!box.empty());

        this->reset();
        int totalPrim = this->count_primitive( first, last, box );

        real nonSplitCost = sah_cost(kTCost, kICost, 1.0, totalPrim, 0.0, 0.0);

        Split split; // if split.axis < 0 then no need to split
        real bestSplitCost = this->find_candidate_split(box, totalPrim, kTCost, kICost, split);

        if(bestSplitCost < nonSplitCost){
            split = tighten_candidate_split(box, first, last, split);
        }
        else{
            split.axis = -1;
        }

        return split;
    }

private:
    static const int kBinNumber = 32;
    static const real kTCost;  // traversal cost; initialized as 0.1;
    static const real kICost;  // intersect cost; initialized as 0.3;

    Vec2i minBin_[kBinNumber];
    Vec2i maxBin_[kBinNumber];

private:
    inline void reset()
    {
        std::memset( minBin_, 0, sizeof(minBin_) );
        std::memset( maxBin_, 0, sizeof(maxBin_) );
    }

    inline void increaseMinBin( const Vec2i& idx)
    {
        assert( idx.x >= 0 && idx.x <= kBinNumber && idx.y >=0 && idx.y <= kBinNumber );
        ++minBin_[idx.x][0];
        ++minBin_[idx.y][1];
    }

    inline void increaseMaxBin( const Vec2i& idx)
    {
        ++maxBin_[idx.x][0];
        ++maxBin_[idx.y][1];
    }

    inline void accumulateBin()
    {
        size_t i = 0;
        for(; i < kBinNumber - 1; ++i) {
            maxBin_[i] += maxBin_[i-1];
        }
        for(; i > 0; --i) {
            minBin_[i-1] += minBin_[i];
        }
    }

    int count_primitive(PrimitivePtrList::const_iterator primListBeg,
                         PrimitivePtrList::const_iterator primListEnd, const AABB& aabb)
    {
        const Vec2 invBinSize = kBinNumber / aabb.size(); // i.e. == (1.0 / binSize_);
        const size_t maxIdx = kBinNumber - 1;
        int totalPrimCount = 0;

        for( PrimitivePtrList::const_iterator it = primListBeg; it != primListEnd; ++it) {
            ++totalPrimCount;
            const AABB& bb = (*it)->aabb();
            Vec2i begIdx( (bb.lowerbound() - aabb.lowerbound() ) * invBinSize );
            Vec2i endIdx( (bb.upperbound() - aabb.lowerbound() ) * invBinSize );
            begIdx.clamp( 0, maxIdx );
            endIdx.clamp( 0, maxIdx );
            this->increaseMinBin(begIdx);
            this->increaseMaxBin(endIdx);
        }

        accumulateBin();

        return totalPrimCount;
    }

    real find_candidate_split(const AABB& aabb, int totalNumber, real tCost, real iCost, Split& bestSplit )
    {
        // Find the best split candidate using min, max binning information
        Vec2 splitCosts[2] = { Vec2(MAX_FLOAT) };
        Vec2& minCost = splitCosts[0];// use SplitCosts[0] as minCost
        Vec2& newCost = splitCosts[1];
        Vec2i indices[2] = { Vec2i(-1) };
        Vec2i& bestIdx = indices[0];
        Vec2i& newIdx  = indices[1];
        real width  = aabb.w();
        real height = aabb.h();
        Vec2 sideLength( height, width ); // intentionally switch the position of w and h
        real perimeter = ( width + height ); //half perimeter
        perimeter += perimeter; // double it == (width + height)*2
        const Vec2i totalCount(totalNumber);
        Vec2 binSize_ = aabb.size()/kBinNumber;
        Vec2 binSizeDouble = binSize_ + binSize_;
        Vec2 varLength(binSizeDouble);
        for(size_t i = 1, j = 0; i < kBinNumber; ++i, ++j, varLength += binSizeDouble) {
            Vec2 perimeterLeft = sideLength + varLength; // == sideLength +  2* binSize * i;
            Vec2 perimeterRight = perimeter - perimeterLeft;
            Vec2i sharedSize( totalCount - (minBin_[i] + maxBin_[j]) ); // The probability is scaled
            newCost = sah_cost( 0, iCost,
                                perimeterLeft,  maxBin_[j] + sharedSize,
                                perimeterRight, minBin_[i] + sharedSize );
            newIdx = Vec2i(i);
            Vec2i b = lt( newCost, minCost);
            minCost.x = splitCosts[b.x].x;
            minCost.y = splitCosts[b.y].y;
            bestIdx.x = indices[b.x].x;
            bestIdx.y = indices[b.y].y;
        }
        // find best split among x split and y split
        bestSplit.axis = int( minCost.x > minCost.y );
        bestSplit.pos = aabb.lowerbound()[bestSplit.axis] +
                bestIdx[bestSplit.axis] * binSize_[bestSplit.axis];
        return tCost + minCost[bestSplit.axis] / perimeter; // return the unscaled splitting cost
    }

    Split tighten_candidate_split( const AABB& aabb, PrimitivePtrList::const_iterator first, PrimitivePtrList::const_iterator last, Split s){
        Vec2 boundary, bounds = aabb.limit(s.axis);  // bounds[0] = maxLeft, bounds[1] = minRight
        boundary = bounds;
        int nL = 0, nR = 0;
        for(auto it = first; it != last; ++it){
            const Qin::AABB& bb = (*it)->aabb();
            real lb = bb.lowerbound()(s.axis);
            real ub = bb.upperbound()(s.axis);
            assert(lb <= ub);
            if( ub <= s.pos ) { // max end on the left of the split pos
                ++nL;
                if(ub > bounds[0])
                    bounds[0] = ub;
            }
            else if( lb >= s.pos ) { // min end on the right of the split pos
                ++nR;
                if( lb < bounds[1])
                    bounds[1] = lb;
            }
            else {
                ++nR; ++nL;
            }
        }
        s.pos = bounds[ nL < nR ]; // tighten the split

        if( s.pos == boundary[0] || s.pos == boundary[1]) // need not split if the split is at boundary.
        {
            s.axis = -1; // invalid the split
        }

        return s;
    }

    // Surfacee Area Huristics Cost function, scalar version
    inline static real sah_cost( real tCost, real iCost, real probLeft, int cntLeft, real probRight, int cntRight)
    {
        return tCost + iCost*(probLeft*cntLeft + probRight*cntRight);
    }

    // Surfacee Area Huristics Cost function, vector version
    inline static Vec2 sah_cost( real tCost, real iCost, Vec2 probLeft, Vec2i cntLeft, Vec2 probRight, Vec2i cntRight)
    {
        // TODO: reimplement this using SIMD instructions
        return tCost + iCost*( probLeft * cntLeft + probRight * cntRight);
    }
};

///////////////////////// KdTree2DNode //////////////
/// \brief The KdTree2DNode class
/// Rather than hide this class inside KdTree2D, many properties are exposed for easier debugging.
/// Unique_ptr rather than raw pointer is used. It saves lots of effort in destructor.
/// The primitive ptr list hold shared resources from other component, e.g., a pool of primitives.
class KdTree2DNode
{
    friend class KdTree2DLazy;
public:
    KdTree2DNode() :
        leftChild_(nullptr),
        rightChild_(nullptr)
    {
        split_.axis = -1;
    }

    KdTree2DNode(const AABB& box, PrimitivePtrList&& p) :
        aabb_(box),
        leftChild_(nullptr),
        rightChild_(nullptr),
        primList_(p)
    {
        split_.axis = -1;
    }

    inline const Split& split() const {
        return split_;
    }

    inline size_t size() const {
        return std::distance(primList_.begin(), primList_.end());
    }

    inline bool is_splitable(int criterion) const{
        assert(criterion >= 2);
        int i = 0;
        for(auto it = primList_.begin(); it != primList_.end(); ++it){
            ++i;
            if(i >= criterion)
                return true;
        }

        return false;
    }

    const PrimitivePtrList& primitives() const
    {
        return primList_;
    }

    inline bool is_leaf() const {
        return leftChild_ == nullptr && rightChild_ == nullptr;
    }

    inline const std::unique_ptr<KdTree2DNode>& left_child() const {
        return leftChild_;
    }

    inline const std::unique_ptr<KdTree2DNode>& right_child() const {
        return rightChild_;
    }    

    inline std::unique_ptr<KdTree2DNode>& left_child() {
        return leftChild_;
    }

    inline std::unique_ptr<KdTree2DNode>& right_child() {
        return rightChild_;
    }

    inline const AABB& aabb() const  {
        return aabb_;
    }

    void splits(Split& s)
    {
        assert(this->is_leaf());

        if(s.axis < 0)
            return;

        // construct two new child nodes
        // pass parent list to left child, then the nodes are either kept for left child, or spliced/copied to right child
        leftChild_ = std::unique_ptr<KdTree2DNode>(new KdTree2DNode());
        rightChild_ = std::unique_ptr<KdTree2DNode>(new KdTree2DNode());
        leftChild_->primList_.swap(this->primList_);

        // determine owenership of primitives
        PrimitivePtrList::const_iterator it = leftChild_->primList_.begin();
        PrimitivePtrList::const_iterator beforeIt = leftChild_->primList_.before_begin();
        while( it != leftChild_->primList_.end() ) {
            const Qin::AABB& bb = (*it)->aabb();
            real lb = bb.lowerbound()(s.axis);
            real ub = bb.upperbound()(s.axis);
            assert(lb <= ub);
            if( ub <= s.pos ) { // max end on the left of the split pos
                beforeIt = it++;
            }
            else if( lb >= s.pos ) { // min end on the right of the split pos
                ++it;
                rightChild_->primList_.splice_after( rightChild_->primList_.before_begin(),
                                            leftChild_->primList_, beforeIt);
            }
            else { // go across the split
                rightChild_->primList_.push_front( *it ); //copy the node
                beforeIt = it++;
            }
        }

        split_ = s;
        // assign boxes to children by splitting parameter
        aabb_.split( split_, leftChild_->aabb_, rightChild_->aabb_);
    }

private:
    AABB                     aabb_;
    Split                    split_;
    std::unique_ptr<KdTree2DNode> leftChild_;
    std::unique_ptr<KdTree2DNode> rightChild_;
    PrimitivePtrList    primList_;
};

struct IntersectionInfo
{
    const Primitive* primitive; // ptr to the intersected primitive.
    Vec2 t;        // the intersection parameter, in which
                   // t[0] is the absolute distance on the ray;
                   // t[1] is the relative distance on the segment of the primitive
    real crossProduct; // used to detect entering a segement from which side
};

///////////////////////// KdTree2D //////////////////////////////////////////////////////////////////////////////
/// \brief The KdTree2D class
/// Usage: Create a tree with the view port as bounding box, the threshold of node splitting and a node split evaluator.
///        Then populate/repopulate the tree with primitives, which create the root node.
///        intersect function gives the intersection information of a ray and a line segment.
///        The unique pointers themselves will take care of memeory recycling.
class KdTree2D
{
public:
public:
    KdTree2D(const AABB &box, size_t minSplitablePrimitive, SplitEvaluator<PrimitivePtrList::const_iterator>& splitEvaluator )
        : root_(nullptr), aabb_(box), minSplit_( minSplitablePrimitive ), splitEvaluator_(splitEvaluator)
    {
        assert(minSplit_ >= 2);
    }

    template<typename InputIterator>
    void populate(InputIterator first, InputIterator last)
    {
        std::unique_ptr<KdTree2DNode> tmp(new KdTree2DNode(aabb_, PrimitivePtrList(first, last)));
        this->root_.swap(tmp);
    }

    IntersectionInfo intersect(const Ray &ray, const Primitive* lastVisitedPrimitive)
    {
        Vec2 t;
        if( Qin::intersect(ray, this->root_->aabb(), Vec2(0, MAX_FLOAT), t))
        {
            return this->intersect(ray, t, lastVisitedPrimitive, this->root_);
        }

        IntersectionInfo info;
        info.primitive = nullptr;
        return info;
    }

    std::unique_ptr<KdTree2DNode>& root() { return root_;}
    const std::unique_ptr<KdTree2DNode>& root() const { return root_;}

private:
    // An lazy evaluation. Split nodes only when they are hit.
    void build_node(std::unique_ptr<KdTree2DNode>& node){ // lazy build version, used when traverasal the tree
        assert( node != nullptr);
        Split s = this->splitEvaluator_.find_split(node->primitives().begin(), node->primitives().end(), node->aabb());
        node->splits(s);
    }

    // An agressive build function for debug
    void build_tree(std::unique_ptr<KdTree2DNode>& root)
    {
        if(root == nullptr)
            return;

        if(root->is_leaf() && root->is_splitable(minSplit_))
        {
            build_node(root);
            build_tree(root->left_child());
            build_tree(root->right_child());
        }
    }

    IntersectionInfo intersect(const Ray& ray, const Vec2 t, const Primitive* lastVisitedPrimitive, std::unique_ptr<KdTree2DNode>& pTreeNode)
    {
        // assuming 1) pTreeNode not null; 2) they ray intersects, and the range is known as t
        assert(pTreeNode != nullptr);
        // lazy build a node
        if(pTreeNode->is_splitable(this->minSplit_) ){
            this->build_node(pTreeNode);
        }

        if(pTreeNode->is_leaf()){
            // leaf node; intersect with each primitive inside, and find the nearest one
            IntersectionInfo iInfo{nullptr, Vec2(MAX_FLOAT), 0};
            // search over each primitive inside the node
            for(auto it = pTreeNode->primitives().begin(); it != pTreeNode->primitives().end(); ++it){
                if(lastVisitedPrimitive == *it) // if this is the primitive just visited, ignore it to avoid numerical problem
                    continue;

                Vec2 tBox, tPrim;
                real crossProduct;
                if( Qin::intersect(ray, (*it)->aabb(), t, tBox) )
                {
                    //if the ray intersects the AABB of the primitive --> tBox in the range of t on current node's AABB(t)
                    if( Qin::intersect(ray, (*it)->line_seg(), Vec2(0, t[1]), tPrim, crossProduct) ) // Ray line segment intersection test
                    {
                        //and if it really intersects the primitive, in the range of the primitive's AABB(tBox)
                        if( tPrim[0] < iInfo.t[0])
                        {
                            // record the nearest primitive;
                            iInfo.primitive = *it;
                            iInfo.crossProduct = crossProduct;
                            iInfo.t = tPrim;

                        }
                    }
                }
            }

            return iInfo;
        }
        else{
            size_t splitAxis = pTreeNode->split().axis;
            real splitPos = pTreeNode->split().pos;

            real pos0 = ray.origin()[splitAxis] + ray.dir()[splitAxis]*t[0];
            real pos1 = ray.origin()[splitAxis] + ray.dir()[splitAxis]*t[1];
            real tSplit;

            IntersectionInfo iInfo;
            switch( ( int(pos0 < splitPos) << 1 ) + int(pos1 < splitPos) )
            {
            case 0: // pos0 > splitPos and pos1 > splitPos, go right child only
                return intersect(ray, t, lastVisitedPrimitive, pTreeNode->right_child());

            case 1:  // pos0 > splitPos and pos1 < splitPos, go right and then left
                tSplit = ray.t(splitPos, splitAxis);
                iInfo = intersect(ray, Vec2(t[0], tSplit), lastVisitedPrimitive, pTreeNode->right_child());
                if(iInfo.primitive != nullptr)
                    return iInfo;
                else
                    return intersect( ray, Vec2(tSplit, t[1]), lastVisitedPrimitive, pTreeNode->left_child());

            case 2:  // pos0 < splitPos and pos1 > splitPos, go left and the right
                tSplit = ray.t(splitPos, splitAxis);
                iInfo = intersect(ray, Vec2(t[0], tSplit), lastVisitedPrimitive, pTreeNode->left_child());
                if(iInfo.primitive)
                    return iInfo;
                else
                    return intersect( ray, Vec2(tSplit, t[1]), lastVisitedPrimitive, pTreeNode->right_child());

            default: // pos0 < splitPos and pos1 < splitPos, go left child only
                return intersect(ray, t, lastVisitedPrimitive,pTreeNode->left_child());
            }
        }
    }

private:
    std::unique_ptr<KdTree2DNode> root_;
    AABB         aabb_;
    size_t       minSplit_;
    SplitEvaluator<PrimitivePtrList::const_iterator>& splitEvaluator_;
};

}// end of namespace

#endif // KDTREE2D_HPP
