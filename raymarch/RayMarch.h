#ifndef RAYMARCH_H
#define RAYMARCH_H
#include "Light.h"
#include "KdTree2D.h"

namespace Qin{



const Primitive* intersect(LightRayWithState& lightRayWS, KdTree2D& kdTree, IntersectionInfo& iInfo)
{
    Vec2 t;
    KdTree2DNode* pNode;
    if(!lightRayWS.currentNode){
        pNode = kdTree.root();
        if( !intersect(lightRayWS.ray, pNode->aabb(), Vec2(0, MAX_FLOAT), t)) // if no intersection, return NULL
            return NULL;
    }
    else
        pNode = lightRayWS.currentNode;//otherwise, we know light ray is in the root node.

    // now recursively call kdTree.intersect to find the intersecting primitive or return NULL.
    const Primitive* p = kdTree.intersect(lightRayWS, t, pNode, iInfo);
    return p;
}

} // end of namespace Qin
#endif // RAYMARCH_H
