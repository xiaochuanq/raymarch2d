    #ifndef PIPELINE_H
#define PIPELINE_H

#include <QPainter>
#include <string>

enum PipelineInstruction
{
    ANTIALIASE,
    PEN,
    RECT,
    LINE
};

enum PenStyle { // pen style
        NONE,
        SOLID,
        DASH,
        DOT
};

void pipline(const char* fileName, QPainter& painter );

std::string enable_antialiase( bool );

std::string set_pen(float r, float g, float b, int width, int style);

std::string draw_rect(float x1, float y1, float x2, float y2);

std::string draw_line(float x1, float y1, float x2, float y2);

#endif // PIPELINE_H
