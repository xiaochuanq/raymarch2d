#ifndef LIGHTGEOMETRY_H
#define LIGHTGEOMETRY_H

#include "Vec2.h"

namespace Qin
{
inline Vec2 reflect(const Vec2& incident, const Vec2& normal, real negCosI)
{
    return incident + 2*negCosI * normal;
}

inline Vec2 reflect(const Vec2& incident, const Vec2& normal)
{
    real negCosI = -dot(normal, incident);
    return reflect(incident, normal, negCosI);
}

inline Vec2 refract(const Vec2& incident, const Vec2& normal, real negCosI, real n1, real n2)
{   // n1, the incident ray's media
    // n2, the refractor's media
    real n = n1/n2;
    real sinT2 = n * n * (1.0 - negCosI * negCosI);
    if( sinT2 > 1.0)
        return Vec2::ZERO();
    real cosT = sqrt(1.0 - sinT2);
    return n * incident + (n * negCosI - cosT) * normal;
}

inline Vec2 refract(const Vec2& incident, const Vec2& normal, real n1, real n2)
{   // n1, the incident ray's media
    // n2, the refractor's media
    real negCosI = -dot(normal, incident);
    return refract(incident, normal, negCosI, n1, n2);
}

inline real refletance_approx(const Vec2& normal, const Vec2& incident, real n1, real n2)
{   // Schilk's approximation to Fresnel equation
    real r0 = (n1 - n2) / (n1 + n2);
    r0 *= r0;
    real cosI = -dot( normal, incident);
    if( n1 > n2)
    {
        real n = n1/n2;
        real sinT2 = n * n * (1.0 - cosI * cosI);
        if( sinT2 > 1.0)
            return 1.0;
        cosI = sqrt(1.0 - sinT2);
    }
    real x = 1.0 - cosI;
    real X = x*x;
    return r0 + (1.0 - r0) * X * X * x;
}

inline real refletance(const Vec2& incident, const Vec2& normal, real n1, real n2)
{
    // Fresnel Equation
    real n = n1/ n2;
    real cosI = -dot(normal, incident);
    real sinT2 = n * n * (1.0 - cosI * cosI);
    if( sinT2 > 1.0)
        return 1.0;
    real cosT = sqrt( 1.0 - sinT2);
    real rOrth = (n1 * cosI - n2 * cosT) / (n1 * cosI + n2 * cosT);
    real rPar = (n2 * cosI - n1 * cosT) / (n2 * cosI + n1 * cosT);
    return (rOrth * rOrth + rPar *rPar) * 0.5;
}

inline real reflect_and_refract(const Vec2& incident, const Vec2& normal, real negCosI, real n1, real n2, Vec2& reflect, Vec2& refract)
{
    real n = n1 / n2;
    reflect = incident + 2 * negCosI * normal;
    real sinT2 = n * n * (1.0 - negCosI * negCosI);
    if( sinT2 > 1.0) { // TIR
        refract = Vec2::ZERO();
        return 1.0;
    }
    real cosT = sqrt( 1.0 - sinT2);
    real m = (n * negCosI - cosT) ;
    refract = n * incident + m * normal;
    // Not to use approximation
    // real rOrth = (n1 * cosI - n2 * cosT) / (n1 * cosI + n2 * cosT);
    real rOrth = m * n2 / (n1 * negCosI + n2 * cosT);
    real rPar = (n2 * negCosI - n1 * cosT) / (n2 * negCosI + n1 * cosT);
    return (rOrth * rOrth + rPar *rPar) * 0.5;
}

inline real reflect_and_refract(const Vec2& incident, const Vec2& normal, real n1, real n2, Vec2& reflect, Vec2& refract)
{
    real negCosI = -dot(normal, incident);
    return reflect_and_refract(incident, normal, negCosI, n1, n2, reflect, refract);
}

inline real reflect_and_refract_approx(const Vec2& incident, const Vec2& normal, real cosI, real n1, real n2, Vec2& reflect, Vec2& refract)
{
    real n = n1 / n2;
    reflect = incident + 2 * cosI * normal;
    real sinT2 = n * n * (1.0 - cosI * cosI);
    if( sinT2 > 1.0) { // TIR
        refract = Vec2::ZERO();
        return 1.0;
    }
    real cosT = sqrt( 1.0 - sinT2);
    real m = (n * cosI - cosT) ;
    refract = n * incident + m * normal;
    // Use Schilk's approximatation
    real cosX = n > 1 ? cosT : cosI;
    real x = 1.0 - cosX;
    real X = x * x;
    real r0 = (n1 - n2) / (n1 + n2);
    r0 *= r0;
    return r0 + (1.0 - r0)* X*X*x;
}

}


#endif
