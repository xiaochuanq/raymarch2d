/*
-----------------------------------------------------------------------------
This source file is part of OGRE
    (Object-oriented Graphics Rendering Engine)
For the latest info, see http://www.ogre3d.org/

Copyright (c) 2000-2011 Torus Knot Software Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
#ifndef VEC3_H
#define VEC3_H

#include "Common.h"

namespace Qin
{
class  Vec3
{
public:
    real x, y, z;

public:
    inline Vec3()
    {
    }

    inline Vec3( const real fX, const real fY, const real fZ )
        : x( fX ), y( fY ), z( fZ )
    {
    }

    inline explicit Vec3( const real afCoordinate[3] )
        : x( afCoordinate[0] ),
          y( afCoordinate[1] ),
          z( afCoordinate[2] )
    {
    }

    inline explicit Vec3( const int afCoordinate[3] )
    {
        x = (real)afCoordinate[0];
        y = (real)afCoordinate[1];
        z = (real)afCoordinate[2];
    }

    inline explicit Vec3( real* const r )
        : x( r[0] ), y( r[1] ), z( r[2] )
    {
    }

    inline explicit Vec3( const real scaler )
        : x( scaler )
        , y( scaler )
        , z( scaler )
    {
    }


    /** Exchange the contents of this vector with another.
    */
    inline void swap(Vec3& other)
    {
        std::swap(x, other.x);
        std::swap(y, other.y);
        std::swap(z, other.z);
    }

    inline real operator [] ( const size_t i ) const
    {
        assert( i < 3 );
        return *(&x+i);
    }

    inline real& operator [] ( const size_t i )
    {
        assert( i < 3 );
        return *(&x+i);
    }
    inline real operator () ( const size_t i ) const
    {
        assert( i < 3 );
        return *(&x+i);
    }

    inline real& operator () ( const size_t i )
    {
        assert( i < 3 );
        return *(&x+i);
    }
    /// Pointer accessor for direct copying
    inline real* ptr()
    {
        return &x;
    }
    /// Pointer accessor for direct copying
    inline const real* ptr() const
    {
        return &x;
    }

    inline Vec3& operator = ( const Vec3& rkVector )
    {
        x = rkVector.x;
        y = rkVector.y;
        z = rkVector.z;

        return *this;
    }

    inline Vec3& operator = ( const real fScaler )
    {
        x = fScaler;
        y = fScaler;
        z = fScaler;

        return *this;
    }

    inline bool operator == ( const Vec3& rkVector ) const
    {
        return ( x == rkVector.x && y == rkVector.y && z == rkVector.z );
    }

    inline bool operator != ( const Vec3& rkVector ) const
    {
        return ( x != rkVector.x || y != rkVector.y || z != rkVector.z );
    }

    // arithmetic operations
    inline Vec3 operator + ( const Vec3& rkVector ) const
    {
        return Vec3(
                   x + rkVector.x,
                   y + rkVector.y,
                   z + rkVector.z);
    }

    inline Vec3 operator - ( const Vec3& rkVector ) const
    {
        return Vec3(
                   x - rkVector.x,
                   y - rkVector.y,
                   z - rkVector.z);
    }

    inline Vec3 operator * ( const real fScalar ) const
    {
        return Vec3(
                   x * fScalar,
                   y * fScalar,
                   z * fScalar);
    }

    inline Vec3 operator * ( const Vec3& rhs) const
    {
        return Vec3(
                   x * rhs.x,
                   y * rhs.y,
                   z * rhs.z);
    }

    inline Vec3 operator / ( const real fScalar ) const
    {
        assert( fScalar != 0.0 );
        real fInv = 1.0f / fScalar;
        return Vec3(
                   x * fInv,
                   y * fInv,
                   z * fInv);
    }

    inline Vec3 operator / ( const Vec3& rhs) const
    {
        return Vec3(
                   x / rhs.x,
                   y / rhs.y,
                   z / rhs.z);
    }

    inline const Vec3& operator +() const
    {
        return *this;
    }

    inline Vec3 operator -() const
    {
        return Vec3(-x, -y, -z);
    }

    // overloaded operators to help Vec3
    inline friend Vec3 operator * ( const real fScalar, const Vec3& rkVector )
    {
        return Vec3(
                   fScalar * rkVector.x,
                   fScalar * rkVector.y,
                   fScalar * rkVector.z);
    }

    inline friend Vec3 operator / ( const real fScalar, const Vec3& rkVector )
    {
        return Vec3(
                   fScalar / rkVector.x,
                   fScalar / rkVector.y,
                   fScalar / rkVector.z);
    }

    inline friend Vec3 operator + (const Vec3& lhs, const real rhs)
    {
        return Vec3(
                   lhs.x + rhs,
                   lhs.y + rhs,
                   lhs.z + rhs);
    }

    inline friend Vec3 operator + (const real lhs, const Vec3& rhs)
    {
        return Vec3(
                   lhs + rhs.x,
                   lhs + rhs.y,
                   lhs + rhs.z);
    }

    inline friend Vec3 operator - (const Vec3& lhs, const real rhs)
    {
        return Vec3(
                   lhs.x - rhs,
                   lhs.y - rhs,
                   lhs.z - rhs);
    }

    inline friend Vec3 operator - (const real lhs, const Vec3& rhs)
    {
        return Vec3(
                   lhs - rhs.x,
                   lhs - rhs.y,
                   lhs - rhs.z);
    }

    // arithmetic updates
    inline Vec3& operator += ( const Vec3& rkVector )
    {
        x += rkVector.x;
        y += rkVector.y;
        z += rkVector.z;

        return *this;
    }

    inline Vec3& operator += ( const real fScalar )
    {
        x += fScalar;
        y += fScalar;
        z += fScalar;
        return *this;
    }

    inline Vec3& operator -= ( const Vec3& rkVector )
    {
        x -= rkVector.x;
        y -= rkVector.y;
        z -= rkVector.z;

        return *this;
    }

    inline Vec3& operator -= ( const real fScalar )
    {
        x -= fScalar;
        y -= fScalar;
        z -= fScalar;
        return *this;
    }

    inline Vec3& operator *= ( const real fScalar )
    {
        x *= fScalar;
        y *= fScalar;
        z *= fScalar;
        return *this;
    }

    inline Vec3& operator *= ( const Vec3& rkVector )
    {
        x *= rkVector.x;
        y *= rkVector.y;
        z *= rkVector.z;

        return *this;
    }

    inline Vec3& operator /= ( const real fScalar )
    {
        assert( fScalar != 0.0 );

        real fInv = 1.0f / fScalar;

        x *= fInv;
        y *= fInv;
        z *= fInv;

        return *this;
    }

    inline Vec3& operator /= ( const Vec3& rkVector )
    {
        x /= rkVector.x;
        y /= rkVector.y;
        z /= rkVector.z;

        return *this;
    }

    inline real length () const
    {
        return std::sqrt( x * x + y * y + z * z );
    }

    inline real squared_length () const
    {
        return x * x + y * y + z * z;
    }

    inline real distance(const Vec3& rhs) const
    {
        return (*this - rhs).length();
    }

    inline void pow(real index)
    {
        x = std::pow(x, index);
        y = std::pow(y, index);
        z = std::pow(z, index);
    }

    inline real squared_distance(const Vec3& rhs) const
    {
        return (*this - rhs).squared_length();
    }

    inline real dot(const Vec3& vec) const
    {
        return x * vec.x + y * vec.y + z * vec.z;
    }

    inline real abs_dot(const Vec3& vec) const
    {
        return abs(x * vec.x) + abs(y * vec.y) + abs(z * vec.z);
    }

    inline real normalize()
    {
        real fLength = std::sqrt( x * x + y * y + z * z );
        // Will also work for zero-sized vectors, but will change nothing
        if ( fLength > 1e-08 )
        {
            real fInvLength = 1.0f / fLength;
            x *= fInvLength;
            y *= fInvLength;
            z *= fInvLength;
        }

        return fLength;
    }

    inline Vec3 cross( const Vec3& rkVector ) const
    {
        return Vec3(
                   y * rkVector.z - z * rkVector.y,
                   z * rkVector.x - x * rkVector.z,
                   x * rkVector.y - y * rkVector.x);
    }

    inline Vec3 mid_point( const Vec3& vec ) const
    {
        return Vec3(
                   ( x + vec.x ) * 0.5f,
                   ( y + vec.y ) * 0.5f,
                   ( z + vec.z ) * 0.5f );
    }

    inline bool operator < ( const Vec3& rhs ) const
    {
        if( x < rhs.x && y < rhs.y && z < rhs.z )
            return true;
        return false;
    }

    inline bool operator > ( const Vec3& rhs ) const
    {
        if( x > rhs.x && y > rhs.y && z > rhs.z )
            return true;
        return false;
    }

    inline void make_floor( const Vec3& cmp )
    {
        if( cmp.x < x ) x = cmp.x;
        if( cmp.y < y ) y = cmp.y;
        if( cmp.z < z ) z = cmp.z;
    }

    inline void make_ceil( const Vec3& cmp )
    {
        if( cmp.x > x ) x = cmp.x;
        if( cmp.y > y ) y = cmp.y;
        if( cmp.z > z ) z = cmp.z;
    }

    inline Vec3 perpendicular(void) const
    {
        static const real fSquareZero = (real)(1e-06 * 1e-06);
        Vec3 perp = this->cross( Vec3::UNIT_X() );
        // Check length
        if( perp.squared_length() < fSquareZero )
        {
            /* This vector is the Y axis multiplied by a scalar, so we have
               to use another axis.
            */
            perp = this->cross( Vec3::UNIT_Y() );
        }
        perp.normalize();

        return perp;
    }

    inline real angle_between(const Vec3& dest)
    {
        real lenProduct = length() * dest.length();

        // Divide by zero check
        if(lenProduct < 1e-6f)
            lenProduct = 1e-6f;

        real f = dot(dest) / lenProduct;

        f = clamp(f, (real)-1.0, (real)1.0);
        return std::acos(f);

    }

    inline bool zero_length(void) const
    {
        real sqlen = (x * x) + (y * y) + (z * z);
        return (sqlen < (EPSILON * EPSILON));

    }

    inline Vec3 normalized_copy(void) const
    {
        Vec3 ret = *this;
        ret.normalize();
        return ret;
    }

    inline Vec3 reflect(const Vec3& normal) const
    {
        return Vec3( *this - ( 2 * this->dot(normal) * normal ) );
    }

    inline bool position_equals(const Vec3& rhs, real tolerance = 1e-03) const
    {
        return real_equal(x, rhs.x, tolerance) &&
               real_equal(y, rhs.y, tolerance) &&
               real_equal(z, rhs.z, tolerance);

    }

    inline bool position_closes(const Vec3& rhs, real tolerance = 1e-03f) const
    {
        return squared_distance(rhs) <=
               (squared_length() + rhs.squared_length()) * tolerance;
    }


    inline bool is_nan() const
    {
        return std::isnan(x) || std::isnan(y) || std::isnan(z);
    }

    static inline Vec3 ZERO() {
        return Vec3(0.0, 0.0, 0.0);
    }
    static inline Vec3 UNIT_X() {
        return Vec3(1.0, 0.0, 0.0);
    }
    static inline Vec3 UNIT_Y() {
        return Vec3(0.0, 1.0, 0.0);
    }
    static inline Vec3 UNIT_Z() {
        return Vec3(0.0, 0.0, 1.0);
    }

    inline const Vec2& as_vec2() const{
        return *((const Vec2*)this);
    }

};

inline Vec3 min( const Vec3& v1, const Vec3& v2) {
    return Vec3( std::min(v1.x, v2.x), std::min(v1.y, v2.y), std::min(v1.z, v2.z) );
}

inline Vec3 max( const Vec3& v1, const Vec3& v2) {
    return Vec3( std::max(v1.x, v2.x), std::max(v1.y, v2.y), std::max(v1.z, v2.z) );
}

inline Vec3 cross( const Vec3& v1, const Vec3& v2) {
    return Vec3(
               v1.y * v2.z - v1.z * v2.y,
               v1.z * v2.x - v1.x * v2.z,
               v1.x * v2.y - v1.y * v2.x);
}

inline real dot( const Vec3& v1, const Vec3& v2) {
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

inline std::ostream& operator << ( std::ostream& o, const Vec3& v )
{
    o << "Vec3(" << v.x << ", " << v.y << ", " << v.z << ")";
    return o;
}

}
#endif
