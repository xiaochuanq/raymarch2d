#ifndef LINESEGMENT_H
#define LINESEGMENT_H
#include "Vec2.h"

namespace Qin {

class LineSegment
{
public:
    LineSegment() {}
    LineSegment(const Vec2& s, const Vec2& e) : start_(s), end_(e) { }
    const Vec2& start ()         const {
        return start_;
    }
    const Vec2& end   ()         const {
        return end_;
    }
    const Vec2& point (size_t i) const {
        assert(is_binary(i));
        return (&start_)[i];
    }
    Vec2 midpoint() const{
        return (start_ + end_)*0.5;
    }

    Vec2  normal() const {
        Vec2 n = vector().rskew();
        n.normalize();
        return n;
    }
    Vec2  vector() const {
        return end_ - start_;
    }
public:
    Vec2 start_;
    Vec2 end_;
};

} //namespace Qin

#endif
