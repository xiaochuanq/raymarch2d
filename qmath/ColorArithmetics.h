#ifndef COLORARITHMETICS_H
#define COLORARITHMETICS_H

#include "Common.h"
#include "Vec3.h"
/*
 pixel level = 255*luminance(1/gamma) ;    luminance = 0.5.
Your monitor's gamma should be 2.2 for Windows or 1.8 for old Mac.

algorithm of wavelenth to rgb: RGB VALUES FOR VISIBLE WAVELENGTHS   by Dan Bruton (astro@tamu.edu)
 */
namespace Qin
{

typedef Vec3 Color3;

inline Color3 rgb_lookup(int waveLength)
{
    switch( (waveLength - 350) / 10)
    {
    case 0: //350 ~440, ie [0, 9)
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
        return Color3((440.0 - waveLength ) / (440 - 350), 0.0, 1.0);
    case 9: //440 ~ 490
    case 10:
    case 11:
    case 12:
    case 13:
        return Color3(0.0, (waveLength - 440.0) / (490 - 440), 1.0);
    case 14: // 490 ~510
    case 15:
        return Color3( 0.0, 1.0, (510-waveLength)/(510-490) );
    case 16: // 510 ~ 580
    case 17:
    case 18:
    case 19:
    case 20:
    case 21:
    case 22:
        return Color3((waveLength - 510.0) / (580 - 510), 1.0, 0.0);
    case 23: //580 ~ 645 , use 650 to approximate
    case 24:
    case 25:
    case 26:
    case 27:
    case 28:
    case 29:
        return Color3(1.0, (650.0-waveLength)/(650-580), 0.0);
    default: // > 645 ~ 780
        return Color3(1.0, 0.0, 0.0);
    }
}

inline real factor_lookup(int waveLength)
{
    switch( (waveLength - 140) / 280)
    {
    case 0: // < 420
        return 0.3 + 0.7*(waveLength - 350.0) / (420 - 350);
    case 1: // 420 ~ 700
        return 1.0;
    default: // > 700
        return 0.3 + 0.7*(780.0 - waveLength) / (780 - 700);
    }
}

inline Color3 wavelength2rgb_int(int waveLength)
{
    Color3 color = rgb_lookup(waveLength);
    real factor = factor_lookup(waveLength);
    color *= factor;
//    color.pow(2.4);
    return color;
}


inline Color3 wavelength2rgb_real(real waveLength)//gamma == 1.0*
{
    Color3 color(0.0); // color[0], color[1], and color[2]
    if(waveLength >= 380 && waveLength < 440)
    {
        color[0]	= -(waveLength - 440) / (440 - 380);
        color[1] = 0.0;
        color[2] = 1.0;
    }
    else if(waveLength >= 440 && waveLength < 490)
    {
        color[0]	= 0.0;
        color[1] = (waveLength - 440) / (490 - 440);
        color[2] = 1.0;
    }
    else if(waveLength >= 490 && waveLength < 510)
    {
        color[0] = 0.0;
        color[1] = 1.0;
        color[2] = -(waveLength - 510) / (510 - 490);
    }
    else if(waveLength >= 510 && waveLength < 580)
    {
        color[0] = (waveLength - 510) / (580 - 510);
        color[1] = 1.0;
        color[2] = 0.0;
    }
    else if(waveLength >= 580 && waveLength < 645)
    {
        color[0] = 1.0;
        color[1] = -(waveLength - 645) / (645 - 580);
        color[2] = 0.0;
    }
    else if(waveLength >= 645 && waveLength < 781)
    {
        color[0] = 1.0;
        color[1] = 0.0;
        color[2] = 0.0;
    }

    real factor = 0.0;
    if(waveLength >= 380 && waveLength < 420)
    {
        factor = 0.3 + 0.7*(waveLength - 380) / (420 - 380);
    }
    else if(waveLength >= 420 && waveLength < 701)
    {
        factor = 1.0;
    }
    else if(waveLength >= 701 && waveLength < 781)
    {
        factor = 0.3 + 0.7*(780 - waveLength) / (780 - 700);
    }
    return color * factor;
}


inline void gamma_correct(Color3& color, real gamma)
{
    color.pow( gamma );
}


/*
real sellmeier(int w, const Vec3& B, const Vec3& C, real A = 1.0)
int w2 = w * w;
return A + dot(B, 1/(w2 - C)) * w2;
}

template<size_t N>
void attenuate(const std::array<real, N>& phonto0, std::array<real, N>& photon1, real a, real distance )
{   //if alpha = 1, then opaque, factor = 1/exp(distance), attenuate rapidly; if alpha=0, transparent, factor = 1, no attenuation
    real factor = std::exp( -a*distance);
    std::transform(photon0.begin(), photon0.end(), photon1.begin(), [](real c) {
        return c * factor;
    } );
}

template<size_t N>
void attuenuate(std::array<real, N>& photon, real a, real distance )
{
    real factor = std::exp( -a*distance);
    std::for_each(photon.begin(), photon.end(), [](real& c) {
        c *= factor;
    } );
}


template<size_t N>
void reflect(const std::array<real, N>& phonto0, const std::array<real, N>& filter, real r, std::array<real, N>& photon1 )
{
    std::transform(photon0.begin(), photon0.end(), filter.begin(), photon1.begin(), [](real a, real b ) {
        return a*b*r
    });
}

template<size_t N>
void reflect(const std::array<real, N>& phonto0, const std::array<real, N>& filter, std::array<real, N>& photon1 )
{
    std::transform(photon0.begin(), photon0.end(), filter.begin(), photon1.begin(), [](real a, real b ) {
        return a*b
    });
}

void split(const std::array<real, N>& phonto0, real r, real t, std::array<real, N>& photon_r, std::array<real, N>& photon_t )
{
    for( size_t i = 0; i < N; ++i) {
        photon_r[i] = photon0[i]*r;
        photon_t[i] = photon0[i]*t;
    }
    // std::transform(photon0.begin(), photon0.end(), photon_r.begin(), [](real c) { return c * r; } );
    // std::transform(photon0.begin(), photon0.end(), photon_t.begin(), [](real c) { return c * t; } );
}
*/
}
#endif
