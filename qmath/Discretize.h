#ifndef DISCRETIZE_H
#define DISCRETIZE_H
#include <vector>
#include "Common.h"
#include "Vec2.h"
#include "Circle.h"
#include "Parabola.h"
#include "LineSegment.h"
namespace Qin
{

template<typename Curve>
size_t discretize(const Curve& c,
                  Vec2* vertex_pool,
                  Vec2* normal_pool,
                  const Vec2& range,
                  size_t numSeg)
{
    assert(numSeg > 0);
    real delta = ( range[1] - range[0] ) / numSeg;
    LineSegment seg(c.point_at(range[0]), c.point_at(range[0]+delta));
    Vec2 n = seg.normal();
    real normal_factor = 1.0;
    if(n.dot(c.normal_at(range[0]+0.5*delta)) < 0)
        normal_factor = -1.0;
    size_t i = 0;
    real t = range[0];
    for( ; i < numSeg; ++i, t += delta){
        vertex_pool[i] = c.point_at(t);
        normal_pool[i] = c.normal_at(t) * normal_factor;
    }
    vertex_pool[i++] = c.point_at( range[1] );
    normal_pool[i++] = c.normal_at( range[1] ) * normal_factor;
    return numSeg + 1;
}

size_t discretize(const std::vector<Vec2>& polyline,
                  Vec2* vertex_pool,
                  Vec2* normal_pool)
{
    int j = 0;
    for( size_t i = 1; i < polyline.size(); ++i)
    {
        LineSegment seg(polyline[i-1], polyline[i]);
        Vec2 n = seg.normal();
        vertex_pool[j] = seg.start();
        normal_pool[j++] = n;
        vertex_pool[j] = seg.end();
        normal_pool[j++] = n;
    }
    return j;
}

}
#endif // DISCRETIZE_H
