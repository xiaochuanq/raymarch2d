#include <map>
#include <string>

enum Material
{
    DIOMAND,
    FUSEDSILICA,
    ICE,
    SAPPHIRE,
    SCHOTTBK7,
    SCHOTTSF2,
    SCHOTTSF10,
    SCHOTTSF11,
    SI,
    WATER
};

struct SellmeierCoeff
{
    real A;
    Vec3 B;
    Vec3 C;
};

std::map<std::string, SellmeierCoeff> coeff;
void populate_coeff(std::map<std::string, SellmeierCoeff>& coeff)
{   //unit um
    coeff["AMTIR-1"] = {1.0, {}, {} };
//n = sqrt( 1 + 5.298*pow(x,2)/(pow(x,2)-pow(0.29007,2)) + 0.6039*pow(x,2)/(pow(x,2)-pow(32.022,2)) )
    coeff["AMTIR-3"] = {1.0, {}, {} };
// n = sqrt( 1 + 5.8505*pow(x,2)/(pow(x,2)-pow(0.29192,2)) + 1.4536*pow(x,2)/(pow(x,2)-pow(42.714,2)) )
    coeff["FusedSilica"] = {1.0, {}, {} };
//n = sqrt( 1 + 0.6961663*pow(x,2)/(pow(x,2)-pow(0.0684043,2)) + 0.4079426*pow(x,2)/(pow(x,2)-pow(0.1162414,2)) + 0.8974794*pow(x,2)/(pow(x,2)-pow(9.896161,2)) )

    coeff["Diomand"] = { 1.0, {4.3356, 0.3306, 0.0}, {0.1060*0.1060, 0.1750*0.1750, 0.0} } ;
//n = sqrt( 1 + 4.3356*pow(x,2)/(pow(x,2)-pow(0.1060,2)) + 0.3306*pow(x,2)/(pow(x,2)-pow(0.1750,2)) )
    coeff["Si"] = {1.0, {10.6684293, 0.003043475, 1.54133408}, {0.301516485*0.301516485, 1.13475115*1.13475115, 1104.0*1104.0 } };
//n = sqrt( 1 + 10.6684293*pow(x,2)/(pow(x,2)-pow(0.301516485,2)) + 0.003043475*pow(x,2)/(pow(x,2)-pow(1.13475115,2)) + 1.54133408*pow(x,2)/(pow(x,2)-pow(1104.0,2)) )
    coeff["BK7"]) = {1.0, {1.03961212, 0.231792344, 1.01046945}, {00600069867, 0.0200179144, 103.560653} };
//n = sqrt( 1 + 1.03961212*pow(x,2)/(pow(x,2)-0.00600069867) + 0.231792344*pow(x,2)/(pow(x,2)-0.0200179144) + 1.01046945*pow(x,2)/(pow(x,2)-103.560653) )
    coeff["SF10"] = {1.0, {}, {} };
//n = sqrt( 1 + 1.62153902*pow(x,2)/(pow(x,2)-0.0122241457) + 0.256287842*pow(x,2)/(pow(x,2)-0.0595736775) + 1.64447552*pow(x,2)/(pow(x,2)-147.468793) )
    coeff["SF11"] = {1.0, {}, {} };
//n = sqrt( 1 + 1.73759695*pow(x,2)/(pow(x,2)-0.013188707) + 0.313747346*pow(x,2)/(pow(x,2)-0.0623068142) + 1.89878101*pow(x,2)/(pow(x,2)-155.23629) )
    coeff["SF5"] = {1.0, {}, {} };
//n = sqrt( 1 + 1.52481889*pow(x,2)/(pow(x,2)-0.011254756) + 0.187085527*pow(x,2)/(pow(x,2)-0.0588995392) + 1.42729015*pow(x,2)/(pow(x,2)-129.141675) )
    coeff["Sapphire"] = {1.0, {1.50397590, 5.50691410e-1, 6.59273790}, {5.48041129e-3, 1.47994281e-2, 4.02895140e+2} };
// coeff["Water"] = {?, {}, {} }; //http://en.wikipedia.org/wiki/Optical_properties_of_water_and_ice
}
