#ifndef COMMON_H
#define COMMON_H

#include <algorithm>
#include <cassert>
#include <cfloat>
#include <climits>
#include <cmath>
#include <iostream>

namespace Qin {

typedef float real;

const size_t AXIS_X = 0;
const size_t AXIS_Y = 1;
const real EPSILON   = 1.0e-6;
// macros below are defined in <cmath>
const real LN2       = M_LN2;
const real EXP       = M_E;   /* e */
const real PI        = M_PI;   /* pi */
const real TAU       = M_PI*2;   /* 2pi */
const real TWO_PI    = TAU;
const real PI_SQUARE = PI * PI;
const real HALF_PI   = M_PI_2;
const real QUATER_PI = M_PI_2 * 0.5;  /* pi/4 */
const real PI_INV    = M_1_PI;  /* 1/pi */
const real TWO_PI_INV      = M_2_PI;  /* 2/pi */
const real TWO_SQRTPI_INV  = M_2_SQRTPI;   /* 2/sqrt(pi) */
const real SQRT_TWO    = M_SQRT2;   /* sqrt(2) */
const real SQRT_HALF   = M_SQRT1_2;  /* sqrt(1/2), or sqrt(2)/2 */
const real MAX_FLOAT  = FLT_MAX;
const real MIN_FLOAT  = -FLT_MAX;

template<typename T>
inline T clamp(T Value, const T Min, const T Max)
{
    return (Value > Min)? ( Value < Max ? Value : Max) : Min; // In most cases Value won't be clamped
}

inline bool real_equal( real x, real y, real tol = EPSILON)
{
    return std::abs(x-y) < tol;
}

inline bool is_binary( size_t i)
{
    return i == 0 || i == 1;
}

template<typename T>
inline bool in_range(T v, T l, T u)
{
    return v >= l && v <= u;
}

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}
}

#endif // COMMON_H
