#ifndef CIRCLE_H
#define CIRCLE_H
#include "Common.h"
#include "Vec2.h"

namespace Qin {

class Circle { // x^2 + y^2 = r^2
public:
    Circle(real r) : r_(r) { }
    real radius()           const {
        return r_;
    }
    Vec2 point_at(real a)   const {
        return Vec2(std::cos(a)*r_, std::sin(a)*r_);
    }
    Vec2 normal_at(real a)  const {
        return Vec2::unit(a);
    }
private:
    real r_;
};

/*
class CircleClip {
public:
    CircleClip(real r, const Vec2& range)
        : circle_(r), range_(range)
    {
        assert( range[1] - range[0] > 0 &&
                range[1] - range[0] <= TAU );
    }
    const Circle& circle()      const {
        return circle_;
    }
    const Vec2& range()         const {
        return range_;
    }
    void discretize(Vec2* pt_buffer, Vec2* normal_buffer, size_t n){
        assert(n >= 1);
        real delta = (range_[1] - range_[0])/n;
        real a = range_[0];
        for(size_t i = 0; i <= n; ++i, a += delta)
        {
            pt_buffer[i] = circle_.point_at(a);
            normal_buffer[i] = circle_.normal_at(a);
        }
    }
private:
    Circle  circle_;
    Vec2    range_;
};
*/

}




#endif
