#ifndef TRANSFROM_H
#define TRANSFROM_H

#include "Common.h"
#include "Vec2.h"

namespace Qin {
inline Vec2 translate(const Vec2& v, const Vec2& t)
{
    return v + t;
}

inline Vec2 rotate(const Vec2& v, real s, real c)
{
    return Vec2( v.x*c - v.y*s, v.x*s + v.y*c);
}

inline Vec2 rotate(const Vec2& v, real a)
{
    real s = std::sin(a);
    real c = std::cos(a);
    return rotate(v, s, c);
}

inline void rotate(Vec2* v, size_t n, real s, real c)
{
    for( size_t i = 0; i < n; ++i)
    {
        v[i] = rotate( v[i], s, c);
    }
}

inline void rotate(Vec2* v, size_t n, real a)
{
    real s = std::sin(a);
    real c = std::cos(a);
    rotate(v, n, s, c);
}

inline void rotate(const Vec2* vin, Vec2* vout, size_t n, real s, real c)
{
    for( size_t i = 0; i < n; ++i)
    {
        vout[i] = rotate( vin[i], s, c);
    }
}

inline void rotate(const Vec2* vin, Vec2* vout, size_t n, real a)
{
    real s = std::sin(a);
    real c = std::cos(a);
    rotate(vin, vout, n, s, c);
}


inline Vec2 transform(const Vec2& v, const Vec2& t, real a)
{
    return translate( rotate( v, a), t);
}

inline Vec2 transform(const Vec2& v, const Vec2& t, real s, real c)
{
    return translate( rotate(v, s, c), t);
}

inline void transform(Vec2* v, size_t n, const Vec2& t, real s, real c)
{
    for( size_t i = 0; i < n; ++i)
    {
        v[i] = transform( v[i], t, s, c);
    }
}

inline void transform(Vec2* v, size_t n, const Vec2& t, real a)
{
    real s = std::sin(a);
    real c = std::cos(a);
    transform(v, n, t, s, c);
}

inline void transform(const Vec2* vin, Vec2* vout, size_t n, const Vec2& t, real s, real c)
{
    for( size_t i = 0; i < n; ++i)
    {
        vout[i] = transform( vin[i], t, s, c);
    }
}

inline void transform(const Vec2* vin, Vec2* vout, size_t n, const Vec2& t, real a)
{
    real s = std::sin(a);
    real c = std::cos(a);
    transform(vin, vout, n, t, s, c);
}

}
#endif
