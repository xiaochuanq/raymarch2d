#ifndef MATH_H
#define MATH_H

#include "Common.h"
#include "AABB.h"
#include "Circle.h"
#include "ColorArithmetics.h"
#include "Intersection.h"
#include "LightGeometry.h"
#include "LineSegment.h"
#include "Numerical.h"
#include "Ray.h"
#include "Vec2.h"
#include "Transform.h"

#endif // MATH_H
