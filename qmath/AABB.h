#ifndef AABB_H
#define AABB_H

#include "Common.h"
#include "LineSegment.h"
#include "Vec2.h"

namespace Qin {

struct Split
{
    int axis;
    real pos;
};

class AABB
{
public:
    AABB( real l = 0, real b = 0, real r = 0, real t = 0) {
        // To guarantee efficiency, the ctors won't check validity.
        lowerbound_.x = l;
        lowerbound_.y = b;
        upperbound_.x = r;
        upperbound_.y = t;
        assert( valid() );
    }
    AABB( const Vec2& lb, const Vec2& ub) : lowerbound_(lb), upperbound_(ub)
    {
        assert( valid() );
    }

    real h() const {
        return upperbound_.y - lowerbound_.y;
    }
    real w() const {
        return upperbound_.x - lowerbound_.x;
    }

    inline bool empty() const {
        return upperbound_.y == lowerbound_.y || upperbound_.x == lowerbound_.x;
    }

    const Vec2& lowerbound() const {
        return lowerbound_;
    }
    const Vec2& upperbound() const {
        return upperbound_;
    }
    const Vec2 bound(size_t i) const {
        assert(is_binary(i) );
        return (&lowerbound_)[i];
    }

    Vec2 limit( size_t axis) const {
        assert(is_binary(axis) );
        return Vec2(lowerbound_(axis), upperbound_(axis));
    }
    // axis: x or y

    real size(size_t axis) const {
        assert(is_binary(axis) );
        return upperbound_(axis) - lowerbound_(axis);
    }
    Vec2 size() const {
        return upperbound_ - lowerbound_;
    }

    real area() const {
        return h() * w();
    }

    void split(const Split& s, AABB& left_box, AABB& right_box) const {
        assert( in_range(s.pos, lowerbound_(s.axis), upperbound_(s.axis) ) );
        left_box = right_box = *this;
        left_box.upperbound_(s.axis) = s.pos;
        right_box.lowerbound_(s.axis)= s.pos;
        assert( left_box.valid() && right_box.valid() );
    }

    inline bool overlaps( const AABB& box) const {
        return (upperbound_.x >= box.lowerbound_.x) && (lowerbound_.x <= box.upperbound_.x) &&
               (upperbound_.y >= box.lowerbound_.y) && (lowerbound_.y <= box.upperbound_.y);
    }

    bool contains( const Vec2& pt ) const {
        return  lowerbound_.x <= pt.x && upperbound_.x >= pt.x && lowerbound_.y <= pt.x && upperbound_.y >= pt.y;
    }

    bool absolute_contains(Vec2 pt) const {
        return  lowerbound_.x < pt.x && upperbound_.x > pt.x && lowerbound_.y < pt.x && upperbound_.y > pt.y;
    }

    bool operator == (const AABB& aabb) {
        return lowerbound_ == aabb.lowerbound() && upperbound_ == aabb.upperbound();
    }

    bool valid() {
        return lowerbound_.x <= upperbound_.x && lowerbound_.y <= upperbound_.y;
    }
private:
    Vec2 lowerbound_;
    Vec2 upperbound_;
};

inline bool overlap(const AABB& box1, const AABB box2)
{
    return box1.overlaps(box2);
}

inline AABB aabb(const Vec2& v1, const Vec2& v2)
{
    return AABB(std::min(v1.x, v2.x),  //left
                std::min(v1.y, v2.y),  //bottom
                std::max(v1.x, v2.x),  //right
                std::max(v1.y, v2.y) );//top
}


inline AABB aabb(const LineSegment& seg)
{
    return aabb(seg.start(), seg.end());
}

inline AABB aabb(const Vec2* v, size_t n)
{
    assert( n >= 1);
    real xmin = v[0].x;
    real ymin = v[0].y;
    real xmax = xmin;
    real ymax = ymin;
    for( size_t i = 1; i < n; ++i) {
        const real& x = v[i].x;
        const real& y = v[i].y;
        if( x < xmin)
            xmin = x;
        else if(x > xmax)
            xmax = x;
        if( y < ymin)
            ymin = y;
        else if( y > ymax)
            ymax = y;
    }
    return AABB( xmin, ymin, xmax, ymax);
}

} //namespace QIN
#endif // AABB_H
