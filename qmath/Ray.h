#ifndef RAY_H
#define RAY_H

#include "LineSegment.h"
#include "Vec2.h"
namespace Qin {

class BasicRay {
public:
    BasicRay(const Vec2& o, real theta) : origin(o), dir(Vec2::unit(theta)) { }
    explicit BasicRay( const Vec2& o, const Vec2& d) : origin(o), dir(d) { }
public:
    Vec2 origin;
    Vec2 dir;
};

class Ray{
public:
    Ray( const Vec2& o, real theta) : origin_(o), dir_(Vec2::unit(theta))
    {
        invDir_ = 1/dir_;
        sign_ = (invDir_.x < 0) | (invDir_.y < 0) << 1;
    }

    explicit Ray( const Vec2& o, const Vec2& d) : origin_(o), dir_(d)
    {
        // d must be a unit vector. The release version won't verify nor nomalize for performance
        std::cout << "direction vector length "<<  dir_.length() <<std::endl;
        assert( abs( dir_.length() - 1 ) < 0.001 );
        invDir_ = 1/dir_;
        sign_ = (invDir_.x < 0) | (invDir_.y < 0) << 1;
    }
    inline void origin( const Vec2& o){
        origin_ = o;
    }

    inline void dir(const Vec2& d){
        dir_ = d;
        invDir_ = 1/d;
        sign_ = (invDir_.x < 0) | (invDir_.y < 0) << 1;
    }

    inline const Vec2& origin() const {
        return origin_;
    }

    inline const Vec2& dir() const {
        return dir_;
    }

    inline int sign(int axis)      const {
        return (sign_ & (1 << axis)) >> axis ;
    }
    inline const Vec2& inv_dir()   const {
        return invDir_;
    }

    inline Vec2 at(real t) const {
        return origin_ + dir_ * t;
    }

    inline real t(real coord, size_t axis) const
    {
        return (coord - origin_[axis]) * invDir_[axis];
    }

    inline real t(const Vec2& pt) const
    {
        return (pt[0] - origin_[0]) * invDir_[0];
    }

    inline LineSegment segment(const Vec2& limit) const {
        return LineSegment(
                   origin_ + dir_ * limit[0],
                   origin_ + dir_ * limit[1] );
    }

    inline LineSegment segment(real t0, real t1) const {
        return LineSegment(
                    origin_ + dir_ * t0,
                    origin_ + dir_ * t1  );
    }

private:
    Vec2 origin_;
    Vec2 dir_;
    Vec2 invDir_;
    int  sign_;
};

} // namespace Qin

#endif // RAY_H
