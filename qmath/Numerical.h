#ifndef NUMERICAL_H
#define NUMERICAL_H
#include "Common.h"

namespace Qin {

namespace QNumerical {
inline double pow(double a, double b)
{
    union
    {
        double d;
        int x[2];
    } u = { a };
    u.x[1] = (int)(b * (u.x[1] - 1072632447) + 1072632447);
    u.x[0] = 0;
    return u.d;
}

#define _2p23 8388608.0f // == pow(2, 23)

class Float32PowerLookup
{
public:
    /**
     * * @precision  number of mantissa bits used, >= 0 and <= 18
     */
    Float32PowerLookup(unsigned int precision) :
        tableSize_(1<<precision),
        mantissaOffset_(23 - precision)
    {
        pTable_ = new unsigned int[tableSize_];
        powFastSetTable();
    }

    ~Float32PowerLookup() {
        delete pTable_;
    }

    /**
     * Get pow (fast!).
     *
     * @val        power to raise radix to
     * @ilog2      one over log, to required radix, of two
     *
     *
     * for pow( 2, val), ilog2 = 1 / log2(2) = 1
     * for pow( e, val), ilog2 = 1 / log(2) = 1.44269504088896
     * for pow(10, val), ilog2 = 1 / log10(2) = 3.32192809488736
     * for pow( r, val), ilog2 = log(r) / log(2) = log(r) * 1.44269504088896
     */
    float operator()(float r, float val)
    {
        // the three conditional branches below is optional for special accuracy. It lower the speed about 10%.
        if( val == 0.0)
            return 1.0;
        if( val == 1.0)
            return r;
        if( r == 0.0)
            return 0.0;
        float ilog2 = std::log(r) / (float)LN2;
        /* build float bits */
        int i = (int)( (val * (_2p23 * ilog2)) + (127.0f * _2p23) );
        /* replace mantissa with lookup */
        int it = (i & 0xFF800000) | pTable_[(i & 0x7FFFFF) >> mantissaOffset_];
        /* convert bits to float */
        return *(float*)( &it );
    }
private:
    /**
     * Initialize powFast lookup table.
     */
    void powFastSetTable()
    {
        /* step along table elements and x-axis positions */
        float zeroToOne = 1.0f / ((float)tableSize_ * 2.0f);
        float deltaZeroToOne = 1.0f / (float)(tableSize_);
        for( size_t i = 0;  i < tableSize_;  ++i )
        {
            /* make y-axis value for table element */
            const float f = ((float)pow( 2.0f, zeroToOne ) - 1.0f) * _2p23;
            pTable_[i] = (unsigned int)( f < _2p23 ? f : (_2p23 - 1.0f) );
            zeroToOne += deltaZeroToOne;
        }
    }
private:
    unsigned int* pTable_;
    size_t tableSize_;
    unsigned int mantissaOffset_;
};


inline float rsqrt(float number)
{
    long i;
    float x2, y;
    const float threehalfs = 1.5F;

    x2 = number * 0.5F;
    y  = number;
    i  = * ( long * ) &y;                       // evil floating point bit level hacking
    i  = 0x5f3759df - ( i >> 1 );               // what the fuck?
    y  = * ( float * ) &i;
    y  = y * ( threehalfs - ( x2 * y * y ) );   // 1st iteration
    //y  = y * ( threehalfs - ( x2 * y * y ) );   // 2nd iteration, this can be removed
    return y;
}

inline float rsqrt2(float number)
{
    long i;
    float x2, y;
    const float threehalfs = 1.5F;

    x2 = number * 0.5F;
    y  = number;
    i  = * ( long * ) &y;                       // evil floating point bit level hacking
    i  = 0x5f3759df - ( i >> 1 );               // what the fuck?
    y  = * ( float * ) &i;
    y  = y * ( threehalfs - ( x2 * y * y ) );   // 1st iteration
    y  = y * ( threehalfs - ( x2 * y * y ) );   // 2nd iteration, this can be removed
    return y;
}

inline float sin(float x)
{   // accurate at 0.001
    const float B = 4/PI;
    const float C = -4/(PI_SQUARE);
    float y = B * x + C * x * fabs(x);
    //  const float Q = 0.775;
    const float P = 0.225;
    y = P * (y * fabs(y) - y) + y;   // Q * y + P * y * abs(y)
    return y;
}

inline float cos(float x)
{
    return sin(x + HALF_PI);
}

// Initial AlmostEqualULPs version - fast and simple, but
// some limitations.
inline bool AlmostEqualUlps(float A, float B, int maxUlps)
{
    if (A == B)
        return true;
    int intDiff = abs(*(int*)&A - *(int*)&B);
    if (intDiff <= maxUlps)
        return true;
    return false;
}

// Usable AlmostEqual function
inline bool AlmostEqual2sComplement(float A, float B, int maxUlps)
{
    // Make sure maxUlps is non-negative and small enough that the
    // default NAN won't compare as equal to anything.
    assert(maxUlps > 0 && maxUlps < 4 * 1024 * 1024);
    int aInt = *(int*)&A;
    // Make aInt lexicographically ordered as a twos-complement int
    if (aInt < 0)
        aInt = 0x80000000 - aInt;
    // Make bInt lexicographically ordered as a twos-complement int
    int bInt = *(int*)&B;
    if (bInt < 0)
        bInt = 0x80000000 - bInt;
    int intDiff = abs(aInt - bInt);
    if (intDiff <= maxUlps)
        return true;
    return false;
}

// Support functions and conditional compilation directives for the
// master AlmostEqual function.
#define INFINITYCHECK
#define NANCHECK
#define SIGNCHECK

inline bool IsInfinite(float A)
{
    const unsigned int kInfAsInt = 0x7F800000;

    // An infinity has an exponent of 255 (shift left 23 positions) and
    // a zero mantissa. There are two infinities - positive and negative.
    if ((*(int*)&A & 0x7FFFFFFF) == kInfAsInt)
        return true;
    return false;
}

inline bool IsNan(float A)
{
    // A NAN has an exponent of 255 (shifted left 23 positions) and
    // a non-zero mantissa.
    int exp = *(int*)&A & 0x7F800000;
    int mantissa = *(int*)&A & 0x007FFFFF;
    if (exp == 0x7F800000 && mantissa != 0)
        return true;
    return false;
}

inline int sign(float a)
{
    // The sign bit of a number is the high bit.
    return (*(int*)&a) & 0x80000000;
}



inline int float_as_int(const float& a)
{
    assert(sizeof(float) == 4); // appliable to 32 bit float only
    int aInt = *(int*)&a;
    if( aInt < 0 )
        aInt = 80000000 - aInt;
    return aInt;
}

inline int float_lt(float a, float b)
{
    int aInt = float_as_int(a);
    int bInt = float_as_int(b);
    return aInt < bInt;
}

inline int positive_float_lt(float a, float b)
{
    return *(int*)&a < *(int*)&b;
}

inline int float_gt(float a, float b)
{
    int aInt = float_as_int(a);
    int bInt = float_as_int(b);
    return aInt > bInt;
}

inline int positive_float_gt(float a, float b)
{
    return *(int*)&a > *(int*)&b;
}

inline int float_le(float a, float b)
{
    int aInt = float_as_int(a);
    int bInt = float_as_int(b);
    return aInt < bInt;
}

inline int positive_float_le(float a, float b)
{
    return *(int*)&a <= *(int*)&b;
}

inline int float_ge(float a, float b)
{
    int aInt = float_as_int(a);
    int bInt = float_as_int(b);
    return aInt < bInt;
}

inline int positive_float_ge(float a, float b)
{
    return *(int*)&a >= *(int*)&b;
}

inline int boundxing_jmpcode( real lb, real ub, real bound )
{
    return ( lb >= bound) + ( ub <= bound ) << 1;
}


} // end of namespace QMath

} // end of namespace Qin

#endif
