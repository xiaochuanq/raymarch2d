
#ifndef PARABOLA_H
#define PARABOLA_H

namespace Qin {

class ParabolaYX2 { // y = ax^2
public:
    ParabolaYX2(real a ) : a_(a) { }
    Vec2 point_at(real x) const {
        return Vec2(x, a_ * x * x);
    }
    Vec2 normal_at( real x ) const {
        Vec2 v(-2*a_*x, 1);
        return v.normalized_copy();
    }
public:
    real a_;
};

typedef ParabolaYX2 Parabola;

class ParabolaXY2 {
public:
    ParabolaXY2( real a ) : a_(a) { }
    Vec2 point_at( real y ) const {
        return Vec2( a_*y*y, y);
    }
    Vec2 normal_at( real y ) const {
        Vec2 v(1, -2*a_*y);
        return v.normalized_copy();
    }
public:
    real a_;
};

/*
class ParabolaClip {
public:
    ParabolaClip( real a, const Vec2& range ) : para_(a), range_(range)
    {
    }
    const Parabola& parabola()  const {
        return para_;
    }
    const Vec2& range()         const {
        return range_;
    }
private:
    Parabola para_;
    Vec2 range_;
};
*/
}

#endif
