/*
 -----------------------------------------------------------------------------
 This source file is part of OGRE
 (Object-oriented Graphics Rendering Engine)
 For the latest info, see http://www.ogre3d.org/

 Copyright (c) 2000-2011 Torus Knot Software Ltd

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 -----------------------------------------------------------------------------
 */

#ifndef VEC2_H
#define VEC2_H
#include "Common.h"

namespace Qin {

class Vec2 {
public:
    Vec2() {}
    // Construct using coordinates.
    Vec2(real x, real y) : x(x), y(y) {}
    Vec2(const Vec2& v) : x(v.x), y(v.y) {}
    explicit Vec2( real scaler ): x(scaler), y(scaler) {}
    /// Set this vector to all zeros.
    inline void zerofill() {
        x = 0.0f;
        y = 0.0f;
    }

    // Read from and indexed element.
    inline real operator () ( const size_t i ) const {
        return (&x)[i];
    }
    inline real operator [] ( const size_t i ) const {
        return (&x)[i];
    }
    // Write to an indexed element.
    inline real& operator () ( const size_t i ) {
        return (&x)[i];
    }
    inline real& operator [] ( const size_t i ) {
        return (&x)[i];
    }

    inline Vec2& operator = ( const Vec2& v )
    {
        x = v.x;
        y = v.y;
        return *this;
    }

    inline Vec2& operator = ( const real s)
    {
        x = s;
        y = s;
        return *this;
    }

    inline bool operator == ( const Vec2& v ) const
    {
        return ( x == v.x && y == v.y );
    }

    inline bool operator != ( const Vec2& v ) const
    {
        return ( x != v.x || y != v.y  );
    }

    // arithmetic operations
    inline Vec2 operator + ( const Vec2& v ) const
    {
        return Vec2(
                    x + v.x,
                    y + v.y);
    }

    inline Vec2 operator - ( const Vec2& v ) const
    {
        return Vec2(
                    x - v.x,
                    y - v.y);
    }

    inline Vec2 operator * ( const real s ) const
    {
        return Vec2(
                    x * s,
                    y * s);
    }

    inline Vec2 operator * ( const Vec2& rhs) const
    {
        return Vec2(
                    x * rhs.x,
                    y * rhs.y);
    }

    inline Vec2 operator / ( const real s ) const
    {
        real fInv = 1.0f / s;
        return Vec2( x * fInv, y * fInv);
    }

    inline Vec2 operator / ( const Vec2& rhs) const
    {
        return Vec2(
                    x / rhs.x,
                    y / rhs.y);
    }

    inline const Vec2& operator + () const
    {
        return *this;
    }

    inline Vec2 operator - () const
    {
        return Vec2(-x, -y);
    }

    // overloaded operators to help Vec2
    inline friend Vec2 operator * ( const real s, const Vec2& v )
    {
        return Vec2(
                    s * v.x,
                    s * v.y);
    }

    inline friend Vec2 operator / ( const real s, const Vec2& v )
    {
        return Vec2(
                    s / v.x,
                    s / v.y);
    }

    inline friend Vec2 operator + (const Vec2& lhs, const real rhs)
    {
        return Vec2(
                    lhs.x + rhs,
                    lhs.y + rhs);
    }

    inline friend Vec2 operator + (const real lhs, const Vec2& rhs)
    {
        return Vec2(
                    lhs + rhs.x,
                    lhs + rhs.y);
    }

    inline friend Vec2 operator - (const Vec2& lhs, const real rhs)
    {
        return Vec2(
                    lhs.x - rhs,
                    lhs.y - rhs);
    }

    inline friend Vec2 operator - (const real lhs, const Vec2& rhs)
    {
        return Vec2(
                    lhs - rhs.x,
                    lhs - rhs.y);
    }
    // arithmetic updates
    inline Vec2& operator += ( const Vec2& v )
    {
        x += v.x;
        y += v.y;

        return *this;
    }

    inline Vec2& operator += ( const real s )
    {
        x += s;
        y += s;

        return *this;
    }

    inline Vec2& operator -= ( const Vec2& v )
    {
        x -= v.x;
        y -= v.y;

        return *this;
    }

    inline Vec2& operator -= ( const real s )
    {
        x -= s;
        y -= s;

        return *this;
    }

    inline Vec2& operator *= ( const real s )
    {
        x *= s;
        y *= s;

        return *this;
    }

    inline Vec2& operator *= ( const Vec2& v )
    {
        x *= v.x;
        y *= v.y;

        return *this;
    }

    inline Vec2& operator /= ( const real s )
    {
        real fInv = 1.0f / s;
        x *= fInv;
        y *= fInv;
        return *this;
    }

    inline Vec2& operator /= ( const Vec2& v )
    {
        x /= v.x;
        y /= v.y;
        return *this;
    }

    inline void swap(Vec2& other)
    {
        std::swap(x, other.x);
        std::swap(y, other.y);
    }

    inline void swap()
    {
        std::swap(x, y);
    }

    inline real length () const
    {
        return sqrt( x * x + y * y );
    }

    inline real squared_length () const
    {
        return x * x + y * y;
    }

    inline real distance(const Vec2& rhs) const
    {
        return (*this - rhs).length();
    }

    inline real squared_distance(const Vec2& rhs) const
    {
        return (*this - rhs).squared_length();
    }

    inline real dot(const Vec2& vec) const
    {
        return x * vec.x + y * vec.y;
    }

    inline real normalize()
    {
        real fLength = sqrt( x * x + y * y);
        // Will also work for zero-sized vectors, but will change nothing
        if ( fLength > 1e-08 )
        {
            real fInvLength = 1.0f / fLength;
            x *= fInvLength;
            y *= fInvLength;
        }
        return fLength;
    }

    /** Returns a vector at a point half way between this and the passed
                in vector.  */
    inline Vec2 mid_point( const Vec2& vec ) const
    {
        return Vec2(
                    ( x + vec.x ) * 0.5f,
                    ( y + vec.y ) * 0.5f );
    }

    /** Returns true if the vector's scalar components are all greater
                that the ones of the vector it is compared against. */
    inline bool operator < ( const Vec2& rhs ) const
    {
        if( x < rhs.x && y < rhs.y )
            return true;
        return false;
    }

    /** Returns true if the vector's scalar components are all smaller
                that the ones of the vector it is compared against. */
    inline bool operator > ( const Vec2& rhs ) const
    {
        if( x > rhs.x && y > rhs.y )
            return true;
        return false;
    }

    inline void make_floor( const Vec2& cmp )
    {
        if( cmp.x < x ) x = cmp.x;
        if( cmp.y < y ) y = cmp.y;
    }

    inline void make_ceil( const Vec2& cmp )
    {
        if( cmp.x > x ) x = cmp.x;
        if( cmp.y > y ) y = cmp.y;
    }

    inline Vec2 perpendicular(void) const
    {
        return Vec2 (-y, x);
    }

    inline real cross( const Vec2& v ) const
    {
        return x * v.y - y * v.x;
    }

    /** Returns true if this vector is zero length. */
    inline bool zero_length(void) const
    {
        real sqlen = (x * x) + (y * y);
        return (sqlen < (1e-06 * 1e-06));

    }

    inline Vec2 normalized_copy(void) const
    {
        Vec2 ret = *this;
        ret.normalize();
        return ret;
    }

    inline Vec2 reflect(const Vec2& normal) const // normal is the normal of the mirror plane
    {
        return Vec2( *this - ( 2 * this->dot(normal) * normal ) );
    }

    inline Vec2 skew() const
    {
        return Vec2(-y, x);
    }

    inline Vec2 rskew() const
    {
        return Vec2(y, -x);
    }

    inline bool is_nan() const
    {
        return std::isnan(x) || std::isnan(y);
    }

    inline Vec2 abs() const {
        return Vec2( fabs(x), fabs(y) );
    }
public:
    // non member functions and constants
    inline static Vec2 unit( real angle ) {
        return Vec2( std::cos(angle), std::sin(angle));
    }
    inline static Vec2 ZERO()   {
        return Vec2(0.0, 0.0);
    }
    inline static Vec2 UNIT_X() {
        return Vec2(1.0, 0.0);
    }
    inline static Vec2 UNIT_Y() {
        return Vec2(0.0, 1.0);
    }
public:
    real x, y;
};

inline Vec2 min( const Vec2& v1, const Vec2& v2) {
    return Vec2( std::min(v1.x, v2.x), std::min(v1.y, v2.y) );
}

inline Vec2 max( const Vec2& v1, const Vec2& v2) {
    return Vec2( std::max(v1.x, v2.x), std::max(v1.y, v2.y) );
}

inline real cross( const Vec2& v1, const Vec2& v2) {
    return v1.x * v2.y - v1.y * v2.x;
}

inline real dot( const Vec2& v1, const Vec2& v2) {
    return v1.x * v2.x + v1.y * v2.y;
}

inline std::ostream& operator<< (std::ostream& out, const Vec2& v)
{
    out<<"Vec2("<<v.x<<", "<<v.y<<")";
    return out;
}

struct Vec2i{
    Vec2i() {}
    explicit Vec2i(int s) : x(s), y(s){ }

    Vec2i(int ix, int iy) : x(ix), y(iy) { }

    Vec2i(const Vec2& v): x(int(v.x)), y(int(v.y)) { }

    operator Vec2 () { return Vec2((real)x, (real)y); }
    
    inline Vec2i& operator+=( int i ){
        x += i;
        y += i;
        return *this;
    }
    inline Vec2i& operator+=( const Vec2i& v ){
        x += v.x;
        y += v.y;
        return *this;
    }
    inline Vec2i& operator-=( int i ){
        x -= i;
        y -= i;
        return *this;
    }
    inline Vec2i& operator-=( const Vec2i& v ){
        x -= v.x;
        y -= v.y;
        return *this;
    }
    inline Vec2i& operator=( const Vec2i& v){
        x = v.x;
        y = v.y;
        return *this;
    }
    inline int operator () ( const size_t i ) const {
        return (&x)[i];
    }
    inline int operator [] ( const size_t i ) const {
        return (&x)[i];
    }
    inline int& operator () ( const size_t i ) {
        return (&x)[i];
    }
    inline int& operator [] ( const size_t i ) {
        return (&x)[i];
    }

    inline void clamp( int lb, int ub)
    {
        x = Qin::clamp(x, lb, ub);
        y = Qin::clamp(y, lb, ub);
    }

    int x;
    int y;
};

inline Vec2i operator+(const Vec2i& v1, const Vec2i& v2)
{
    Vec2i v3(v1);
    v3 += v2;
    return v3;
}

inline Vec2i operator-(const Vec2i& v1, const Vec2i& v2)
{
    Vec2i v3(v1);
    v3 -= v2;
    return v3;
}

inline Vec2i gt( const Vec2& v1, const Vec2& v2){
    return Vec2i(v1.x > v2.x, v1.y > v2.y);
}

inline Vec2i ge( const Vec2& v1, const Vec2& v2){
    return Vec2i(v1.x >= v2.x, v1.y >= v2.y);
}

inline Vec2i lt( const Vec2& v1, const Vec2& v2){
    return Vec2i( v1.x < v2.x, v1.y < v2.y);
}

inline Vec2i le( const Vec2& v1, const Vec2& v2){
    return Vec2i(v1.x <= v2.x, v1.y <= v2.y);
}

inline Vec2i eq( const Vec2& v1, const Vec2& v2){
    return Vec2i(v1.x == v2.x, v1.y == v2.y);
}

} //namespace Qin

#endif
