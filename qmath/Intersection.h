#ifndef INTERSECTION_H
#define INTERSECTION_H
#include "AABB.h"
#include "LineSegment.h"
#include "Numerical.h"
#include "Ray.h"

namespace Qin {

inline bool intersect(const LineSegment& seg1, const LineSegment& seg2, Vec2& t)
{
    Vec2 v1 = seg1.vector();
    Vec2 v2 = seg2.vector();
    real d = cross(v1, v2); // this also defines which side segment 1 is pass through segment 2
    if( std::fabs(d) < EPSILON)// if parallel
        return false;
    // Intersection
    Vec2 xDiff = seg2.start() - seg1.start();
    real a = cross( xDiff, v2);
    real b = cross( xDiff, v1);
    real dInv = 1/d;
    t[0] = a * dInv; //  t on seg1
    t[1] = b * dInv; //  t on seg2
    if( t[0] < 0 || t[0] > 1 || t[1] < 0 || t[1] > 1 )
        return false;
    return true;
}

inline bool intersect(const Ray& r, const LineSegment& seg1, const Vec2& limit, Vec2& t) {
    LineSegment seg0 = r.segment(limit);
    Vec2 v0 = seg0.vector();
    Vec2 v1 = seg1.vector();
    real d = cross(v0, v1); // this also defines which side segment 1 is pass through segment 2
    if( std::fabs(d) < EPSILON)// if parallel
        return false;
    // Intersection
    Vec2 xDiff = seg1.start() - seg0.start();
    real a = cross( xDiff, v1);
    real b = cross( xDiff, v0);
    t[0] = a + limit[0];    // absolute t ( >= 0) on seg0 (i.e., the ray )
    t[1] = b /d;            // relative t (in [0,1]) on seg1
    if( t[0] < limit[0] || t[0] > limit[1] || t[1] < 0 || t[1] > 1 )
        return false;
    return true;
}

inline bool intersect(const Ray& r, const LineSegment& seg1, const Vec2& limit, Vec2& t, real& d) {
    LineSegment seg0 = r.segment(limit);
    Vec2 v0 = seg0.vector();
    Vec2 v1 = seg1.vector();
    d = cross(v0, v1); // this also defines which side segment 1 is pass through segment 2
    if( std::fabs(d) < EPSILON)// if parallel
        return false;
    // Intersection
    Vec2 xDiff = seg1.start() - seg0.start();
    real dInv = 1/d;
    real a = cross( xDiff, v1);
    real b = cross( xDiff, v0);
    t[0] = a * dInv * (limit[1]-limit[0]) + limit[0];    // absolute t ( >= 0) on seg0 (i.e., the ray )
    t[1] = b * dInv;            // relative t (in [0,1]) on seg1
    if( t[0] < limit[0] || t[0] > limit[1] || t[1] < 0 || t[1] > 1 )
        return false;
    return true;
}

inline bool intersect(const Ray& r, const LineSegment& seg, const Vec2& limit, Vec2& pt, Vec2& t) {
    LineSegment seg2 = r.segment(limit);
    if( intersect(seg2, seg, t) ) {
        pt = seg.start()*(1-t(1)) + seg.end()*t(1);
        t(0) = (pt.x - r.origin().x) * r.inv_dir().x;
        return true;
    }
    return false;
}

inline bool intersect(const LineSegment& seg1, const LineSegment& seg2, Vec2& t, real& d)
{
    Vec2 v1 = seg1.vector();
    Vec2 v2 = seg2.vector();
    d = cross(v1, v2); // this also defines which side segment 1 is pass through segment 2
    if( std::fabs(d) < EPSILON)// if parallel
        return false;
    // Intersection
    Vec2 xDiff = seg2.start() - seg1.start();
    real a = cross( xDiff, v2);
    real b = cross( xDiff, v1);
    real dInv = 1/d;
    t[0] = a * dInv; //  t on seg1
    t[1] = b * dInv; //  t on seg2
    if( t[0] < 0 || t[0] > 1 || t[1] < 0 || t[1] > 1 )
        return false;
    return true;
}

inline bool intersect(const Ray& r, const LineSegment& seg, const Vec2& limit, Vec2& pt, Vec2& t, real& d) {
    LineSegment seg2 = r.segment(limit);
    if( intersect(seg2, seg, t, d) ) {
        pt = seg.start()*(1-t(1)) + seg.end()*t(1);
        t(0) = (pt.x - r.origin().x) * r.inv_dir().x;
        return true;
    }
    return false;
}

inline bool intersect(const Ray& r, const AABB& aabb, const Vec2& limit, Vec2& t) {
    /*
     * "An Efficient and Robust Ray–Box Intersection Algorithm"
     * Amy Williams Steve Barrus R. Keith Morley Peter Shirley
     * University of Utah
     */
    t[0] =  (aabb.bound(  r.sign(0)).x - r.origin().x) * r.inv_dir().x;
    t[1] =  (aabb.bound(1-r.sign(0)).x - r.origin().x) * r.inv_dir().x;
    real tymin = (aabb.bound(  r.sign(1)).y - r.origin().y) * r.inv_dir().y;
    real tymax = (aabb.bound(1-r.sign(1)).y - r.origin().y) * r.inv_dir().y;
    if ( (t[0] > tymax) || (tymin > t[1]) )
        return false;
    if (tymin > t[0])
        t[0] = tymin;
    if (tymax < t[1])
        t[1] = tymax;
    return ( (t[0] <= limit[1]) && (t[1] >= limit[0]) );
}

} //namespace Qin

#endif // INTERSECTION_H
