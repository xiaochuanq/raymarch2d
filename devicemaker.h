#ifndef DEVICEMAKER_H
#define DEVICEMAKER_H
#include <fstream>
#include "raymarch/KdTree2D.h"
#include "raymarch/Primitive.h"
#include "raymarch/Device.h"
#include "raymarch/RayMarch.h"
#include "kdtreemaker.h"

Qin::DeviceFactory& theDeviceFactory(){
    static Qin::DeviceFactory* pdf = 0;
    if( !pdf){
        pdf = new Qin::DeviceFactory(10240);
    }
    return *pdf;
}

/*
template<typename PrimitiveConstIterator>
void populate_kdtree(KdTree2D& kdTree,
                     PrimitiveConstIterator beg,
                     PrimitiveConstIterator end,
                     bool lazyBuild = true)
{
    assert( kdTree.root()->leaf() );
    for( PrimitiveConstIterator it = beg; it != end; ++it)
        kdTree.root()->add_prim_ptr(&(*it));
    clock_t tEnd, tStart = clock();
    if( !lazyBuild)
        kdTree.build_tree( kdTree.root() );
    tEnd = clock();
    std::cout.unsetf(std::ios::floatfield);
    std::cout.precision(12);
    std::cout << (double)(tEnd - tStart)/CLOCKS_PER_SEC<<"sec of kdTree population"<<std::endl;
}
*/

void populate_kdtree(Qin::KdTree2D& kdTree,
                     const std::list<Qin::Device*>& devList,
                     bool lazyBuild = false)
{
    assert( kdTree.root()->leaf() );
    std::cout <<" Primitive added to the kdTree" <<std::endl;
    size_t n = 0;
    for(std::list<Qin::Device*>::const_iterator it = devList.begin(); it != devList.end(); ++it ){
        for(const Qin::Primitive* ptr = (*it)->primitive_beg(); ptr != (*it)->primitive_end(); ++ptr){
            std::cout << ptr->end_point(0)<<"-->"<<ptr->end_point(1)<<std::endl;
            kdTree.root()->add_prim_ptr(ptr);
            ++n;
        }
    }
    std::cout << "Total # " << n << std::endl;
    clock_t tEnd, tStart = clock();
    if(!lazyBuild)
        kdTree.build_tree( kdTree.root() );
    tEnd = clock();
    std::cout.unsetf(std::ios::floatfield);
    std::cout.precision(12);
    std::cout << (double)(tEnd - tStart)/CLOCKS_PER_SEC<<"sec of kdTree building"<<std::endl;
}

void append_to_pipeline_file(const char *fileName, const std::vector<Qin::LineSegment>& segs)
{
    std::ofstream ofs(fileName, std::ios_base::app);
    ofs << enable_antialiase(true);
    ofs << set_pen(0, 1.0, 0.0, 0.0, SOLID);
    for(int i =0; i < segs.size(); ++i)
        ofs << draw_line(segs[i].start().x, segs[i].start().y, segs[i].end().x, segs[i].end().y);
    ofs.close();
}

void create_and_draw_devices_and_light_ray_and_kd_tree(){

    const char* fileName = "./kdtree_drawing.pipe";
    theDeviceFactory().reset_buffer();

    std::list<Qin::Device*> *pDevList = theDeviceFactory().create_devices();
    Qin::KdTree2D kdTree( Qin::AABB(0, 0, 1600, 900), 10, Qin::binning_split_evaluate);
    int i = 0;
    for(std::list<Qin::Device*>::iterator it = pDevList->begin(); it != pDevList->end(); ++it, ++i)
        (*it)->transform_primitive(Qin::Vec2(200+300*i, 200), Qin::PI/6*i);

    populate_kdtree(kdTree, *pDevList, false);

    std::vector<Qin::LightRayWithState> lrays;
    Qin::Vec2 shift(0.0, 1.0);
    Qin::Vec2 origin0(10.0, 400);
    for(int i = -19; i < 18; i+=10){
        Qin::Vec2 origin = origin0 + i * shift;
        int j = 0;
        for( Qin::real theta = -Qin::PI/3; theta < Qin::PI/3; theta += Qin::PI/180, j++){
            if( j == 38)
                lrays.push_back(Qin::LightRayWithState( 550, 1.0, origin, Qin::Vec2::unit(theta)) );
        }
    }

    std::vector<Qin::LineSegment> segments;
    //std::cout << "totally " << lrays.size() << " rays"<<std::endl;
    clock_t tEnd, tStart = clock();
    while(!lrays.empty()){
        //std::cout <<"Ray origin" << lrays.back().ray.origin() <<" dir: "<<lrays.back().ray.dir() << std::endl;
        Qin::LightRayWithState lrws = lrays.back();
        lrays.pop_back();
        Qin::Vec2 rayStartPos = lrws.ray.at(0.0);
        Qin::IntersectionInfo iInfo;
        const Qin::Primitive* p = Qin::intersect(lrws, kdTree, iInfo);
        if( p ){
            lrws.lastVisitedPrimitive = p;
            Qin::Vec2 intersectPos = lrws.ray.at(iInfo.t[0]); //Qin::Vec2 intersectPos = p->position_at(iInfo.t[1]); //
            Qin::LineSegment newSeg(rayStartPos, intersectPos);
            segments.push_back( newSeg);
            if(lrws.photon.intensity >= 0){
                //std::cout << "Intersect at " << intersectPos << std::endl;
                Qin::Vec2 n = p->normal_at(iInfo.t[1]);
                //Qin::real cosI = -dot(n, lrays.back().ray.dir());
                //Qin::Vec2 newDir = Qin::reflect(lrws.ray.dir(), n);
                Qin::Vec2 nReflect, nRefract;
                Qin::real beta = 0.0;
                if( lrws.inAir){
                    beta = Qin::reflect_and_refract(lrws.ray.dir(), n, 1.0, 2.2, nReflect, nRefract);
                    //assert(beta < 1.0);
                    std::cout << "refrectance " << beta <<std::endl;
                }
                else{
                    beta = Qin::reflect_and_refract(lrws.ray.dir(), -n, 2.2, 1.0, nReflect, nRefract);
                    //assert(beta < 1.0);
                    std::cout << "refrectance " << beta <<std::endl;
                }
                //std::cout << "cross product: " <<iInfo.crossProduct <<" and dot product" <<Qin::dot(n, lrws.ray.dir()) << std::endl;
                //assert(iInfo.crossProduct <= 0);
                //if(!std::signbit(iInfo.crossProduct))
                 //   newDir *= -1;
                //std::cout << "Normal "<< n <<" Old dir " << lrws.ray.dir() << " New dir " << newDir << std::endl;
             //   lrws.currentNode = iInfo.pCurrentNode;
                std::cout << "incident" << lrws.ray.dir() <<" normal: "<< n <<" reflect " << nReflect <<" refract " << nRefract<<std::endl;
                lrws.photon.intensity -= 0.1;
                Qin::LightRayWithState lrws1(550, lrws.photon.intensity*beta, intersectPos, nReflect);
                lrws1.inAir = lrws.inAir;
                lrws1.lastVisitedPrimitive = p;
                lrays.push_back(lrws1);
                if( nRefract != Qin::Vec2::ZERO() )
                {
                    Qin::LightRayWithState lrws2(550, lrws.photon.intensity*(1-beta), intersectPos, nRefract);
                    lrws2.inAir = !lrws.inAir;
                    lrws2.lastVisitedPrimitive = p;
                    lrays.push_back(lrws2);
                }
                ////std::cout <<"New Ray origin: " << lrws.ray.origin() << " dir: " << lrws.ray.dir() << std::endl;
            }
        }
        else{
            segments.push_back(lrws.ray.segment(0.0, 10000));
      }
    }
    tEnd = clock();
    std::cout.unsetf(std::ios::floatfield);
    std::cout.precision(12);
    std::cout << (double)(tEnd - tStart)/CLOCKS_PER_SEC<<"sec of intersecting time"<<std::endl;

    create_drawing_pipeline_file(fileName,
                                 *pDevList,
                                 kdTree);
    append_to_pipeline_file(fileName, segments);

    for(std::list<Qin::Device*>::iterator it = pDevList->begin(); it != pDevList->end(); ++it)
        delete (*it);
    delete pDevList;
    theDeviceFactory().reset_buffer();
}



#endif // DEVICEMAKER_H
