HEADERS       = renderarea.h \
                window.h \
                pipeline.h \
    qmath/Vec3.h \
    qmath/Vec2.h \
    qmath/Transform.h \
    qmath/Ray.h \
    qmath/QMath.h \
    qmath/Parabola.h \
    qmath/Numerical.h \
    qmath/MaterialConstants.h \
    qmath/LineSegment.h \
    qmath/LightGeometry.h \
    qmath/Intersection.h \
    qmath/Common.h \
    qmath/ColorArithmetics.h \
    qmath/Circle.h \
    qmath/AABB.h \
    raymarch/KdTree2D.h \
    kdtreemaker.h \
    raymarch/Primitive.h \
    raymarch/Device.h \
    devicemaker.h \
    qmath/Discretize.h \
    raymarch/Material.h \
    raymarch/Light.h \
    lightmaker.h \
    raymarch/RayMarch.h
SOURCES       = main.cpp \
                renderarea.cpp \
                window.cpp \
                pipeline.cpp
RESOURCES     = basicdrawing.qrc

# install
target.path = /Users/xiaochuanq/QtProjects/basicdrawing
sources.files = $$SOURCES $$HEADERS $$RESOURCES $$FORMS basicdrawing.pro images
sources.path = /Users/xiaochuanq/QtProjects/basicdrawing
INSTALLS += target sources
