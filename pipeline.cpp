#include <cmath>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stdexcept>
#include "pipeline.h"

void pipline(const char* fileName, QPainter& painter ){
    painter.setRenderHint(QPainter::Antialiasing, false);
    std::ifstream pipeStream( fileName );
    if( pipeStream.is_open()){
        std::string strBuf;
        try{
            while(  pipeStream.good() ){
                getline(pipeStream, strBuf);
                std::stringstream ss( strBuf );
                int instruction;
                ss >> instruction;
                switch (PipelineInstruction(instruction)){
                case ANTIALIASE:
                {
                    bool a;
                    ss >> a;
                    painter.setRenderHint(QPainter::Antialiasing, a);
                }
                    break;
                case PEN:
                {
                    float r, g, b;
                    int w, s;
                    ss >> r >> g >> b >> w >> s;
                    QColor color(int(r*255), int(g*255), int(b*255));
                    QPen pen( color );
                    pen.setWidth(w);
                    pen.setStyle(Qt::PenStyle(s));
                    painter.setPen(pen);
                }
                    break;
                case RECT:
                {
                    while(ss.good()){
                        QPoint pts[2];
                        for( int i = 0; i< 2; ++i){
                            float x, y;
                            ss >> x >> y;
                            pts[i] = QPoint(floor(x+0.5), floor(y+0.5));
                        }
                        QRect rect(pts[0], pts[1]);
                        painter.drawRect(rect);
                    }
                }
                    break;
                case LINE:
                {
                    while(ss.good()){
                        float x1, y1, x2, y2;
                        ss >> x1 >> y1 >> x2 >> y2;
                        painter.drawLine(floor(x1+0.5), floor(y1+0.5), floor(x2+0.5), floor(y2+0.5));
                    }
                }
                    break;
                default:
                    throw std::invalid_argument ("Drawing Pipeline Instruction");
                }
            }
        }
        catch(...){
            throw std::invalid_argument ("Drawing Pipeline Data Format");
        }
    }
}

std::string enable_antialiase( bool a)
{
    std::stringstream ss;
    ss << ANTIALIASE<< ' ' << a << std::endl;
    return ss.str();
}

std::string set_pen(float r, float g, float b, int width, int style)
{
    std::stringstream ss;
    ss << PEN << ' ' << r <<' ' << g <<' '<< b <<' '<< width <<' '<< style << std::endl;
    return ss.str();
}

std::string draw_rect(float x1, float y1, float x2, float y2)
{
    std::stringstream ss;
    ss << RECT << ' ' << x1 <<' ' << y1 <<' '<< x2 <<' '<< y2  << std::endl;
    return ss.str();
}

std::string draw_line(float x1, float y1, float x2, float y2)
{
    std::stringstream ss;
    ss << LINE << ' ' << x1 <<' ' <<y1<<' '<<x2<<' '<<y2<< std::endl;
    return ss.str();
}
