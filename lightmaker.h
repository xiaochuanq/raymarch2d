#ifndef LIGHTMAKER_H
#define LIGHTMAKER_H

#include "raymarch/Light.h"
#include "pipeline.h"
#include <fstream>
#include <vector>

void create_drawing_pipeline_file(const char *fileName,
                                  const std::vector<Qin::DrawableSegment>& dsv)
{
    std::ofstream ofs(fileName, std::ios_base::app);
    ofs << enable_antialiase(true);
    for(size_t i = 0; i < dsv.size(); ++i)
    {
        const Qin::Color3& clr = dsv[i].clr0;
        ofs << set_pen(clr.x, clr.y, clr.z, 1, SOLID);
        ofs << draw_line(dsv[i].pos0.x, dsv[i].pos0.y, dsv[i].pos1.x, dsv[i].pos1.y);
    }
    ofs.close();
}



#endif // LIGHTMAKER_H
